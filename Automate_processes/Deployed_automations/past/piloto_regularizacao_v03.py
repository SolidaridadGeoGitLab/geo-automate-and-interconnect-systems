import pandas as pd
import geopandas as gpd
import numpy as np
import psycopg2 as pg
import pandas.io.sql as sqlio
from shapely import wkb 
import folium
from folium import plugins
import os
from dataprep.eda import create_report
from pathlib import Path
import warnings
import time
import streamlit_folium
import streamlit as st
from shapely import wkt

df = pd.read_excel('/home/luismellow/Documents/03_Solidaridad_Network/05_selecao_regularizacao_ambiental/01_dataset/xlsx/_43_produtores_v01.xlsx')
df['geometry'] = df['geometry'].apply(wkt.loads)
gdf = gpd.GeoDataFrame(df, crs='epsg:4326')



#Visualize this data using folium
# Create a map object
from streamlit_folium import folium_static

st.set_page_config(layout="wide")

m = folium.Map(location=[-4.44, -50.14],
               zoom_start=11,
               tiles='cartodbpositron',
            zoom_control=True,
            control_scale=True)
# Add the data to the map
#using the tooltip parameter to display the farmer_code


folium.GeoJson(gdf, 
                style_function=lambda x: {'fillColor':'green',
                                            'color': 'green',
                                            'weight' : 2,
                                            'fillOpacity': 0.4},
                
                tooltip=folium.features.GeoJsonTooltip(fields=['nom_munici',
                                                               'cod_imovel',
                                                               'farmer_code'
                                                               ],
                                                       aliases=['Nome do município',
                                                                'Código CAR',
                                                                'farmer_code'
                                                                ],
                                                        localize=True)
                ).add_to(m)

#measure feature
measure_control = plugins.MeasureControl(position='topleft',
                                         active_color='red',
                                         completed_color='red',
                                         primary_length_unit='meters')
#Add a draw polygons tool
draw_polygons  = plugins.Draw(export=False)

choice = gdf['farmer_code'].unique()
#Add a selectbox from streamlit
choice = st.sidebar.selectbox('Selecione o produtor',
                              choice)
#add a zoom to the selected farmer

def convert_df(df):
    return df.to_html().encode('utf-8')


html = convert_df(df)

with open('report.html', 'w') as f:
            f.write(html)


st.download_button(label="download report",
                               data='report.html',
                               file_name="test_report.html")

# Add the measure tool to the map
m.add_child(measure_control)
m.add_child(draw_polygons)
folium.TileLayer('Stamen Terrain').add_to(m)
#folium.TileLayer('MapQuest Open Aerial').add_to(m)
folium.LayerControl().add_to(m)
# Display the map
folium_static(m, width=1500, height=900)