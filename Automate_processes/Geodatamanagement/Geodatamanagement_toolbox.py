import os 
import numpy as np
import shutil
import pandas as pd
from dataprep.eda import create_report
from dataprep.clean import clean_headers
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
from random import choices
from pandas_profiling import ProfileReport
import shutil
import pandas as pd
from unidecode import unidecode



### Definindo a pasta de trabalho, a raiz ###(crtl+l)
def mudando_pasta_de_trabalho(path):
    """ Montando a pasta de trabalho ou pasta raiz e imprimindo os arquivos contidos nela """
    
    
    global root
    os.chdir(path) #mudando a pasta de trabalho
    root =  os.getcwd() #imprimindo a pasta de trabalho
    print('Pasta selecionada como raiz é: ', root)
    print('A pasta contêm os arquivos: \n', os.listdir(root))
    return root

### Separate the file in folders corresponding to the file extension ###

def separate_files_per_extension(path):
    """
    args: path containing the files to be separated
    returns: files separated in folders according to their extension
    
    """
    os.chdir(path)
    for file in os.listdir():
        if os.path.isfile(file):
            file_name, file_extension = os.path.splitext(file)
            #If file extension ends with .shp, .shx, .prj, .dbf it should be in the same folder called shp
            if file_extension in ['.shp', '.shx', '.prj', '.dbf']:
                file_extension = '.shp'
            try:
                os.mkdir(file_extension[1:])
            except FileExistsError:
                pass
            shutil.move(file, file_extension[1:])
    print('Arquivos separados por extensão....')
    for path, subdirs, files in os.walk(root):
            for name in files:
                print(os.path.join(path, name))
    for path, subdirs, files in os.walk(root):
         print("Number of files in", path, ":", len(files))
    return 


### Loop para entrar em cada pasta e criar o relatório de EDA ###    
def A_exploratoria_automatica_arquivos(path, extension):
    """
    Looping em pastas e subpastas para encontrar arquivos com extensão específica e gerar relatório de análise exploratória de dados.
    args: path (str), caminho para a pasta raiz onde estão as pastas com os arquivos a serem analisados
          extension (str), extensão dos arquivos a serem analisados, por exemplo: '.csv', '.shp', '.xlsx', 'geojson'
    
    returns: list of files with the extension and a exploratory data analysis in using dataprep
    
    """
    for root, dirs, files in os.walk(path):
        for file in files:
            #if extension is '.csv' open the file and do the exploratory data analysis
            if file.endswith(extension):
                print(os.path.join(root, file))
                if extension == '.csv':
                    df_csv = pd.read_csv(os.path.join(root, file))
                    #using dataprep.eda to do the exploratory data analysis
                    df_csv = create_report(df_csv, title='Exploratory Data Analysis  -  {}'.format(file))
                if extension == '.xlsx':
                    df_xlsx = pd.read_excel(os.path.join(root, file))
                    report_xslx = create_report(df_xlsx, title='Exploratory Data Analysis   -   {}'.format(file))
                    #Saving the report to folder reports inside the folder xlsx
                    report_xslx.show_browser()    
                if extension == '.shp':
                    df_shp = gpd.read_file(os.path.join(root, file))
                    #Lendo o arquivo shapefile como um dataframe comum
                    df_shp = pd.DataFrame(df_shp.drop(columns=['geometry']))
                    df_shp = create_report(df_shp, title='Exploratory Data Analysis   -   {}'.format(file))
                    df_shp.show_browser()      
                if extension == '.geojson':
                    df_geojson = gpd.read_file(os.path.join(root, file))
                    #Lendo o arquivo shapefile como um dataframe comum
                    df_geojson = pd.DataFrame(df_geojson.drop(columns=['geometry']))
                    df_geojson = create_report(df_geojson, title='Exploratory Data Analysis   -   {}'.format(file))
                    df_geojson.show_browser()
    return 





def dataframe_consistido(caminho):
    """
    Função para limpar todos os arquivos de um diretório
    
    Args:
        caminho (string): caminho para acessar os arquivos em diferentes formatos (xlsx, xls, csv, shp)

    Returns:
        string: arquivos em seus formatos respectivos, consistidos e prontos para análise
    """
    
    os.chdir(caminho)
    for file in os.listdir(caminho):
        if file.endswith(".xlsx") or file.endswith(".xls"):
            #se o diretório 'resultados' não existir, crie
            if not os.path.exists('db_consistido'):
                os.makedirs('db_consistido_xlsx')
        
            #se o diretório 'resultados' existir, exclua e crie novamente
            if os.path.exists('db_consistido_xlsx'):
                shutil.rmtree('db_consistido_xlsx')
                os.makedirs('db_consistido_xlsx')
            
                print('Lendo arquivo...: ', file)
                df_xlsx = pd.read_excel(file, skiprows=0)
                
                print('Limpando arquivo...: ', file)
                #corrigindo os cabeçalhos das colunas
                df_xlsx = df_xlsx.rename(columns = lambda x : x.lower().replace(' ', '_'))
                
                #Retirando espaços vazios antes e depois em cada coluna  
                df_xlsx.applymap(lambda x: x.strip() if isinstance(x, str) else x)
                
                # #Na coluna 'farmer_code' substituir os valores 'nan' por '0'
                df_xlsx['farmer_code'] = df_xlsx['farmer_code'].fillna(0)
                # #Converter o campo 'farmer_code' de object para int64
                df_xlsx['farmer_code'] = df_xlsx['farmer_code'].astype(np.int64)
               
                #Preenchendo os valores nulos ou NaN com 'NODATA'
                for column in df_xlsx.columns:
                    #Transformando todos os campos em strings
                    df_xlsx[column] = df_xlsx[column].astype(str)
                    df_xlsx[column].replace(' ', 'NODATA', inplace=True)
                    df_xlsx[column].replace('nan', 'NODATA', inplace=True)
                    df_xlsx[column].replace('0', 'NODATA', inplace=True)
                
                                        
                # #Salvando o arquivo limpo na pasta cleaned_files_xlsx
                print('Salvando arquivo...: ', file)
                # filename =  file.split('.')[0] + '_limpo.xlsx'
                # df_xlsx.to_excel('resultados/'+filename, index=False)
                print('Arquivo xlsx limpo com sucesso!') 
                return df_xlsx
        
        if file.endswith(".shp"):
                #Criando pasta para os arquivos shapefile limpos
                os.mkdir('db_consistido_shp')
                print('Lendo arquivo...: ', file)
                
                df_shp = gpd.read_file(file, encoding = 'utf-8')
                #corrigindo os cabeçalhos das colunas
                print('Limpando arquivo...: ', file)
                df_shp =  df_shp.rename(columns = lambda x : x.lower().replace(' ', '_'))
                
                
                #Inserindo NODATA quando os valores forem nulos (Padronização de dados nulos)
                for colunas in df_shp.columns:
                    df_shp[colunas] = df_shp[colunas].fillna('NODATA')
                
                for colunas in df_shp.columns:
                    if df_shp[colunas].dtype == 'object':
            #Tornando todos os campos maiusculos
                        df_shp[colunas] = df_shp[colunas].str.upper()
            #Removendo espaços brancos antes e depois de cada string
                        df_shp[colunas] = df_shp[colunas].str.strip()
            #Removendo acentos
                        df_shp[colunas] = df_shp[colunas].apply(unidecode)
                
                #Alterando o nome do arquivo shapefile
                filename =  file.split('.')[0] + '_limpo.shp'
                print('Salvando arquivo...: ', file)
                
                #Movendo arquivo shapefile para a pasta cleaned_files_shp
                df_shp.to_file('db_consistido_shp/'+filename, driver='ESRI Shapefile')
                print('Arquivo shp limpo com sucesso!')
                return df_shp
        else:
            pass                
                
    return 
#df_xlsx  =  dataframe_consistido('/home/luismellow/Documents/08_bd_teste/01_dataset/xlsx/')


def limpando_string_colunas(df):
    """
    Função para transformar todas as strings em minúsculas
    
    Args:
        df (dataframe): dataframe que será utilizado

    Returns:
        dataframe: dataframe com todas as strings em minúsculas
    """
    for col in df.columns:
        if df[col].dtype == 'object':
            #Tornando todos os campos maiusculos
            df[col] = df[col].str.upper()
            #Removendo espaços brancos antes e depois de cada string
            df[col] = df[col].str.strip()
            #Removendo acentos
            df[col] = df[col].apply(unidecode)
            #Convertendo NODATA para NaN
            df[col] = df[col].replace('NODATA', np.nan)
            
    #Exportando df para pasta resultados
    df.to_excel('db_consistido_xlsx/df_consistido.xlsx', index=False)
            
    return df

import psycopg2 as pg
import pandas.io.sql as sqlio
from shapely import wkb 


def connect_to_postgres(query, geodados = False):
    conn = pg.connect(
    host="ec2-3-219-33-146.compute-1.amazonaws.com",
    database="visibilidad",
    user="geoprocessamento",
    password="gprcssmnt%97")
### Cria um cursor para executar comandos SQL
    cursor = conn.cursor()
    ### If this database has a column named 'wkb_geometry', you can use the following line to convert it to a shapely geometry
    if geodados is True:
        geodf = gpd.GeoDataFrame.from_postgis(query, conn, geom_col='wkb_geometry')
        return geodf
    ### else, read the data with pandas and return it
    else:
        df = pd.read_sql(query, conn)
        return df


def lendo_arquivo_por_filename(rootpath, formato_arquivo, filename):
    """
    Objetivo: Ler um arquivo com o formato especificado e retornar um dataframe

    Args:
        rootpath (pathlikeobject): Caminho para a pasta raiz onde os arquivos estão localizados
        formato_arquivo (str): Formato do arquivo que será lido, pois irá acessar a pasta com esse formato
        filename (str): Nome do arquivo que será lido 
    Returns:
        Dataframe: Retorma um dataframe pronto para ser lido
    """
    #Se formato_arquivo for igual a xlsx, então:
    if formato_arquivo == 'xlsx':
        #Lendo o arquivo com o formato especificado       
        # Em formato_arquivo adicionar '/' para que o caminho seja reconhecido 
        df_xslx = pd.read_excel(rootpath + formato_arquivo + '/' + filename)
        return df_xslx
    if formato_arquivo == 'csv':
        #Lendo o arquivo com o formato especificado        
        df_csv = pd.read_csv(rootpath + formato_arquivo + '/'+ filename)
        return df_csv
    if formato_arquivo == 'shp':
        df_shp = gpd.read_file(rootpath + formato_arquivo + '/' + filename)
        return df_shp


def padronizando_cabecalhos(dataframe):
    """
    Objetivo: Função que roda em cada coluna do dataframe e padroniza os cabeçalhos, deixando tudo em minúsculo e sem espaços em branco 

    Args:
        dataframe (dataframe): Dataframe que será utilizado e onde os cabeçalhos serão padronizados

    Returns:
        Dataframe: Dataframe com os cabeçalhos padronizados 
    """
    
    df_padronizado = dataframe.rename(columns = lambda x : x.lower().replace(' ', '_'))
    return df_padronizado

def stripper_empty_space(df, col):
    """
    Objetivo: Remover espaços em branco de uma coluna específica do dataframe
    Args:
        df (dataframe): Dataframe que será utilizado
        col (str): Coluna que será utilizada para remover os espaços em branco

    Returns:
        dataframe: retorna o dataframe com a coluna especificada sem espaços em branco
    """
    
    df[col] = df[col].str.strip()
    df[col] = df[col].str.replace(' ', '') 
    df[col] = df[col].astype('string') 
    return df