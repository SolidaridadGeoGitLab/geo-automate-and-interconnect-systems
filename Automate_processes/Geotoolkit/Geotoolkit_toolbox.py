import geopandas as gpd
import os
import numpy as np
import shapely
import matplotlib.pyplot as plt
from random import choices
import shutil
import pandas as pd
import warnings
import fnmatch
import folium




### Loading test datasets ###
def lista_multi_xlsx(path):
    """ 
    Função para carregar arquivos xslx e retornar uma lista de arquivos xslx
    
    """
    lista_xlsx = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".xlsx"):
                lista_xlsx.append(os.path.join(root, file))

    return lista_xlsx


def lista_multi_shapefiles(path):
    """ 
    Função para carregar arquivos de shapefile e retornar uma lista de shapefiles
    
    """
    lista_shp = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".shp"):
                lista_shp.append(os.path.join(root, file))
    return lista_shp

def medindo_tamanho_arquivos(lista):
    """ 
    Função para medir o tamanho dos arquivos xslx e retornar uma lista com os tamanhos
    
    """
    lista_tamanho = []
    for i in lista:
        lista_tamanho.append(os.path.getsize(i))
    print('Lendo arquivo...{}'.format(i))
    print('Tamanho do arquivo: {} bytes'.format(os.path.getsize(i)))
    return lista_tamanho


### Medindo dimensões dos arquivos ###

def medindo_dimensoes_arquivos(lista):
    
    """
    Função para medir as dimensões dos arquivos e retornar uma lista com as dimensões
    
    """
    lista_dim = []
    
    for itens in lista:
        if itens.endswith(".xlsx"):
            df = pd.read_excel(itens)
            lista_dim.append(df.shape)
            print('Lendo arquivo...{}'.format(itens))
            print('Dimensões do arquivo: {}'.format(df.shape))
        if itens.endswith(".shp"):
            df = gpd.read_file(itens)
            lista_dim.append(df.shape)
            print('Lendo arquivo...{}'.format(itens))
            print('Dimensões do arquivo: {}'.format(df.shape))
    return lista_dim


def mesclando_dataframes_por_farmer_code(arquivo1, arquivo2):
    """
    Args:
        shp (variable): arquivo shapefile contendo os dados geográficos
        xlsx (.xslx): arquivo excel contendo os dados dos produtores a serem analisados

    Returns:
        df_mesclado(Geodataframe): geodataframe contendo os dados dos produtores a serem analisados e seus dados geográficos
    """
    df_mesclado = pd.merge(arquivo1, arquivo2, on='farmer_code')
    return df_mesclado


def trocando_epsg(lista_geodata, epsg, exportar=False):
    """
    Função que troca o sistema de coordenadas de "n" shapefile, de acordo com o epsg informado 

    Args:
        dataframe (Geodataframe): Lista de arquivos shapefile que terão o sistema de coordenadas trocado
        epsg (int): código EPSG do sistema de coordenadas 
        exportar (bool, optional): Se True, exporta os arquivos com o sistema de coordenadas trocado e com sufixo _epsg. Defaults to False.

    Returns:
        dataframe (Geodataframe): geodataframe com o sistema de coordenadas trocado para o epsg informado
    """
    lista_epsg = []
    for i in lista_geodata:
        #Se arquivo contenha o sufixo _epsg, ele não será lido
        if i.endswith("_epsg_{}.shp".format(epsg)):
            pass
        else:
            print('Lendo arquivo shapefile...{}'.format(i), '\n')
            dataframe = gpd.read_file(i)
            print('Sistema de coordenadas atual: {}'.format(dataframe.crs))
            dataframe = dataframe.to_crs(epsg=epsg)
            print('Mudando Sistema de coordenadas para...', epsg, '\n')
            #Append itens to lista_epsg with the new crs, and prefix _epsg
            lista_epsg.append(i)
            ### Opcional, exportando os arquivos com o sistema de coordenadas trocado ###
            if exportar == True:
                dataframe.to_file(i[:-4] + '_epsg_{}.shp'.format(epsg))
                print('Exportando arquivo shapefile...{}'.format(i[:-4] + '_epsg{}.shp'.format(epsg)))
            else:
                pass
        
    return lista_epsg
#trocando_epsg(lista_geodata, 4326, exportar=True)




### Definindo função para realizar operações espaciais ###
def multiferramentas_espaciais_overlay(path, df1, df2, ferramenta, exportar=False):
    """
    Args:
        df1 (_type_): Geopandas dataframe
        df2 (_type_): Geopandas dataframe
        ferramenta (_type_): Ferramenta para realizar operações espaciais, como: intersection, union, difference, symmetric_difference
    """
    ### Intersection ###
    ### Igual a ferramenta Clip utilizada no ArcGIS ###
    
    if ferramenta == 'intersection':
        intersection = gpd.overlay(df1, df2, how='intersection')
        if exportar == True:
            #Exportando arquivo intersectado com o nome do arquivo df1 + _intersection
            intersection.to_file(path + '_intersection{}.shp')
        return intersection
    
    ### Union ###    
    if ferramenta == 'union':
        union = gpd.overlay(df1, df2, how='union')
        if exportar == True:
            union.to_file(path + '_union.shp')
        return union
    
    ### Difference ###
    ### Igual a ferramenta Erase utilizada no ArcGIS ###
    if ferramenta == 'difference':
        difference = gpd.overlay(df1, df2, how='difference')
        if exportar == True:
            difference.to_file(path + '_difference.shp')
        return difference
    
    ### Symmetric Difference ###
    if ferramenta == 'symmetric_difference':
        symmetric_difference = gpd.overlay(df1, df2, how='symmetric_difference')
        if exportar == True:
            symmetric_difference.to_file(path + '_symmetric_difference.shp')
        return symmetric_difference


#path = '/home/luismellow/Documents/08_bd_teste/04_teste_poligonos/'
# for i in lista_multi_shapefiles(path):
#     if fnmatch.fnmatch(i, '*_1_epsg*') :
#         df1 = gpd.read_file(i)
        
#     if fnmatch.fnmatch(i, '*_02_epsg*') :
#         df2 = gpd.read_file(i)
# df_1 = multiferramentas_espaciais_overlay(df1, df2, 'intersection', exportar=False)        
# df_2 = multiferramentas_espaciais_overlay(df1, df2, 'symmetric_difference', exportar=False)


### Medindo área de geometrias ###
def medindo_area_do_poligono(lista, exportando_resultado=False):
    """
    Função para medir a área de geometrias de um shapefile, retornar uma lista com as áreas e adicionar uma coluna com as áreas no geodataframe

    Args:
        lista (list): lista de arquivos shapefile

    Returns:
        lista_area (list): lista com as áreas das geometrias
    """
    if lista == []:
        print('Lista vazia')
    else:
        lista_area = []
        for i in lista:
            df = gpd.read_file(i)
            area = df['geometry'].area
            lista_area.append(area)
            print('Para o arquivo {}, a área total é: {}'.format(i, area), '\n')
            ### Adicionando coluna com as áreas ###
            print('Adicionando coluna com as áreas no geodataframe')
            df['area'] = area
            ### Opcional, exportando arquivo shapefile com a coluna de área ###
            if exportando_resultado == True:
                print('Exportando arquivo shapefile com a coluna de área')
                df.to_file(i[:-4] + '_area.shp')     
    return lista_area



def selecionar_por_localizacao(df1, df2):
    """
    Função para selecao por localização, equivalente ao select by location do ArcGIS ou selecionar por localização do QGIS
    
    """
    subset = df1[df1.within(df2)]
    return subset

#selecionar_por_localizacao(df2, df1)


import subprocess
def universal_conversor(path, df, name, from_format, to_format):
    
    """
    Conversor that uses any format to any other format using
    shapefile as intermediary.
    
    "Esri Shapefile"
    "GeoJSON"
    "json"
    "kml" - obs: if you want to convert to kmz, use kml and change the extension to .kmz

    """
    with open(path + '{}.{}'.format(name, from_format), 'w') as f:
        f.write(df.to_json())
    subprocess.call(['ogr2ogr', '-f', to_format, path + '{}.{}'.format(name, to_format), path + '{}.{}'.format(name, from_format)])
    os.remove(path + '{}.{}'.format(name, from_format))
    return



def convert_json_to_kml(path, df, json_name, kml_name):
    """
    Converts a json file to kml.
    Args: 
    Path: Path to the file
    df: Dataframe to be converted
    json_name: Name of the json file
    kml_name: Name of the kml file
    
    """
    
    with open(str(path + '{}.json'.format(json_name)), 'w') as f:
        f.write(df.to_json())
    subprocess.call(['ogr2ogr', '-f', 'KML', path + '{}.kml'.format(kml_name), path + '{}.json'.format(json_name)])
    os.remove(path + '{}.json'.format(json_name))
    return

from shapely import wkt
def assign_geometry(df, geometry_column):
    
    """
    1) Assigns a geometry column to a dataframe
    2) Converts the geometry column to a geodataframe
    Args: 
    df: Dataframe to be converted
    geometry_column: Name of the column that contains the geometry
    
    """
    
    
    df = df.assign(geometry=df[geometry_column].astype(str).apply(wkt.loads))
    df = gpd.GeoDataFrame(df, geometry='geometry', crs='epsg:4326')
    return df
