///////////////Realizando a análise somente com sentinel-2 /////
////////////// Amostras retiradas manualmente
//Carregar ALOS

//Carregar Sentinel-1 TOA reflectance data/

//Carregar Sentinel-2 TOA reflectance data/

//Carregar Landsat 8 TOA reflectance data/


//Adicionando area de estudo
var area_estudo = aoi_retangulo


//ss
///////////////////////////////////////////////// Funções inserção de variaveis ////////////////////////////////////
//Função para rodar o filtro lee (ALOS 2 -PALSAR-2)
function filterleealos(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Homogenius Class - Enhanced Lee Filter)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLeealos =['HHLee','HVLee']
  var leefiltersalos = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefiltersalos.rename(BandsLeealos));
}

//Função para rodar o filtro lee (Sentinel 1)
function filterlee(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Classe Homogênea - Filtro Lee Melhorado)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLee =['VVLee','VHLee']
  var leefilters = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefilters.rename(BandsLee));
}


//Função para Adicionar o RVI (ALOS 2-PALSAR-2)
function addalosRVI(image)
{
  var rvialos = image.expression(
  '4 * hv / (hv + hh)',
  {
     hh: image.select('HHLee'),    
     hv: image.select('HVLee')
  });
  return image.addBands(rvialos.rename('RVIAlos'));
}
 
//Função para calcular e  Adicionar o RVI (Sentinel 1)
function addS1RVI(image)
{
  var rvi = image.expression(
  '4 * vh / (vh + vv)',
  {
     vv: image.select('VVLee'),    
     vh: image.select('VHLee')
  });
  return image.addBands(rvi.rename('RVI'));
}


//Função para calcular e  Adicionar o NDVI do Landsat 8
function addLandsatNDVI(image)
{
  var Landsat_NDVI = image.expression(
    '((ir-red)/(ir+red))',
    {
      ir: image.select('B5'),
      red: image.select('B4')
    });
    return image.addBands(Landsat_NDVI.rename('LS8_NDVI'));
}


function addNDVIs2(image) {
  var ndvi = image.normalizedDifference(['B8', 'B4']).rename('NDVI');
  return image.addBands(ndvi);
}



//Função para calcular e inseri LAI (Leaf Area Index)
function addLAI(image)
{
    var ndvi = image.expression(
    '((red - ir) / (red + ir))',
    {
        red: image.select('B8'),    
        ir: image.select('B4')});
        
    var LAI = ndvi.expression('0.57 * exp(2.33 * b(0))').rename('LAI');

    image = image.addBands(LAI.rename('LAI'));
return image;
}



//Funçao para GLCM(grey level co-occurrence matrix), utilizada em paisagens heterogêneas para diferenciar
function addGLCMs2 (image){
  var glcm = image.select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'LAI', 
                                                  'QA60']).toUint16().glcmTexture({size: 3})
  image = image.addBands(glcm)                                                
  return image;
}



//Function to Run and add a GLCM algorithm over sentinel 1 image
function addGLCMs1 (image){
  var glcm = image.select(['VV',
                            'VH',
                              'RVI']).toUint16().glcmTexture({size: 3})
  image = image.addBands(glcm)                                                
  return image;
}


// Função para adicionar a análise Tasseled Cap a partir da imagem de Landsat-08
function addTasseledCap (image) {
  var b = image.select("B2", "B3", "B4", "B5", "B6", "B7");
  var brightness_coefficents= ee.Image([0.3029, 0.2786, 0.4733, 0.5599, 0.508, 0.1872])
  var greenness_coefficents= ee.Image([-0.2941, -0.243, -0.5424, 0.7276, 0.0713, -0.1608]);
  var wetness_coefficents= ee.Image([0.1511, 0.1973, 0.3283, 0.3407, -0.7117, -0.4559]);
  
  
  var brightness = image.expression(
          '(B * BRIGHTNESS)',
          {
              'B':b,
              'BRIGHTNESS': brightness_coefficents
              }
          );
  var greenness = image.expression(
  '(B * GREENNESS)',
          {
              'B':b,
              'GREENNESS': greenness_coefficents
              }
          );
  var wetness = image.expression(
  '(B * WETNESS)',
          {
              'B':b,
              'WETNESS': wetness_coefficents
              }
          );
  

brightness = brightness.reduce(ee.call("Reducer.sum"));
greenness = greenness.reduce(ee.call("Reducer.sum"));
wetness = wetness.reduce(ee.call("Reducer.sum"));
  
image = image.addBands(brightness.rename('brightness'));
image = image.addBands(greenness.rename('greenness'));
image = image.addBands(wetness.rename('wetness'));
return image;

}



// Criação das máscara de nuvem usando Sentinel-2 QA60 band.
function maskS2clouds(image) {
  var qa = image.select('QA60');

// Bits 10 e 11 são nuvens e cirrus, respectivamente.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;
  
// Ambos os sinalizadores devem ser definidos como zero, indicando condições claras.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
             qa.bitwiseAnd(cirrusBitMask).eq(0));
  return image.updateMask(mask).divide(10000);
}


//Adicionando indices de vegetação
function addIndices(image){
  return image.addBands([
    image.select('B8','B4').normalizedDifference().multiply(10000).add(10000).rename('NDVI')]);
}

                     
// Fazer o empilhamento de imagens (raster stacking)
var alospalsar =  alos.filterDate('2018-06-01', '2019-10-31')
                  .select(['HH', 'HV'])
                  .map(filterleealos)
                  .map(addalosRVI);



var sentinel1 =  s1.filterDate('2022-06-01', '2022-10-31')
                  .select(['VV', 'VH'])
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VV'))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
                  .filter(ee.Filter.eq('instrumentMode', 'IW'))
                  .filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
                  .map(filterlee)
                  .map(addS1RVI)
                  .map(addGLCMs1);
                  

//Filtrando as imagens do Sentinel-2 para os diferentes anos amostrados                  
var sentinel2_ano_0 = s2.filterDate('2020-06-01', '2020-10-31')
                  .filterMetadata('CLOUDY_PIXEL_PERCENTAGE','less_than', 10)//25 -> 6% de nuvens nas imagens 
                  .select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'QA60'])
                  .map(maskS2clouds)
                  .map(addIndices)
                  .map(addTasseledCap)
                  .map(addLAI)
                  .map(addNDVIs2)
                  .map(addGLCMs2)
                  ;                  
                  


var sentinel2_ano_1 = s2.filterDate('2021-06-01', '2021-10-31')
                  .filterMetadata('CLOUDY_PIXEL_PERCENTAGE','less_than', 10)//25 -> 6% de nuvens nas imagens 
                  .select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'QA60'])
                  .map(maskS2clouds)
                  .map(addIndices)
                  .map(addTasseledCap)
                  .map(addLAI)
                  .map(addNDVIs2)
                  .map(addGLCMs2)
                  ;                  
                  


                  
var sentinel2_ano_2 = s2.filterDate('2022-06-01', '2022-10-31')
                  .filterMetadata('CLOUDY_PIXEL_PERCENTAGE','less_than', 10)//25 -> 6% de nuvens nas imagens 
                  .select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'QA60'])
                  .map(maskS2clouds)
                  .map(addIndices)
                  .map(addTasseledCap)
                  .map(addLAI)
                  .map(addNDVIs2)
                  .map(addGLCMs2)
                  ;
                  
///Filtrando as imagens para a área de estudo, de acordo com anos de coleta
var s2filtered_ano0 = sentinel2_ano_1.median().clip(area_estudo);
var s2filtered_ano1 = sentinel2_ano_1.median().clip(area_estudo);
var s2filtered_ano2 = sentinel2_ano_2.median().clip(area_estudo);

//Adicionando as bandas de cada ano de coleta a uma única imagem empilhada
var s2filtered = s2filtered_ano0.addBands(s2filtered_ano1).addBands(s2filtered_ano2);
//Filtrando as imagens alospalsar e sentinel1 para a área de estudo
var aloslee = alospalsar.median().clip(area_estudo);
var stack = s2filtered.addBands(s1lee).addBands(aloslee);

//Printing the bands of the stack
print('As bandas utilizadas nesta análise são: ', stack.bandNames());

//////// Amostragem  para sentinel-2 ///////////////////////////////////////////////////////
var amostrasAgua = stack.sampleRegions({
  collection: roi_agua,
  scale: 20,
  geometries: true,
  tileScale: 15
}).randomColumn('random').limit(1000, 'random', false)


var amostrasPastagem = stack.sampleRegions({
  collection: pastagem_s2,
  scale: 20,
  geometries: true,
  tileScale: 15
}).randomColumn('random').limit(1000, 'random', false)


var amostrasFloresta = stack.sampleRegions({
  collection: floresta_s2,
  scale: 20,
  geometries: true,
  tileScale: 15
}).randomColumn('random').limit(1250, 'random', false)


var mergeAmostras = amostrasAgua.merge(amostrasPastagem).merge(amostrasFloresta)
mergeAmostras =  mergeAmostras.randomColumn('amostra')

var bands = ["B2", "B3",
              "B4", "B5",
                "B7", "B8",
                    "B8A",
                    "QA60",
                      "LAI",
                      "NDVI",
                      "wetness",
                      "B4_contrast",
                      "B8A_maxcorr",
                      "B3_imcorr1",
                      'VV',
                            'VH',
                              'RVI', "HVLee","HHLee", "RVIAlos" ]
var classe = 'classe'





/////Separando amostras
var datasetTreino = mergeAmostras.filter(ee.Filter.lt('amostra', 0.7))
var datasetTeste = mergeAmostras.filter(ee.Filter.gte('amostra', 0.7))




/////Classificador
var trainedClassifier = ee.Classifier.smileRandomForest({numberOfTrees: 25,
          minLeafPopulation: 6,
          maxNodes: 5}).train({
          features:datasetTreino,
          classProperty: classe,
          inputProperties: bands
});


//Classificando as imagens
var classificada_ano_0 = stack.select(['B2', 'B3', 'B4', 'B5', 'B7', 'B8', 'B8A', 'QA60', 'LAI', 'NDVI', 'wetness', 'B4_contrast', 'B8A_maxcorr', 'B3_imcorr1']).classify(trainedClassifier)

//From the stack, select bands only  sentinel2_ano_1
var classificada_ano_1 = stack.select(['B2_1', 'B3_1', 'B4_1', 'B5_1', 'B7_1', 'B8_1', 'B8A_1', 'QA60_1', 'LAI_1', 'NDVI_1', 'wetness_1', 'B4_contrast_1', 'B8A_maxcorr_1', 'B3_imcorr1_1']).classify(trainedClassifier)

//From the stacl, select bands only sentinel2_ano_2
var classificada_ano_2 = stack.select(['B2_2', 'B3_2', 'B4_2', 'B5_2', 'B7_2', 'B8_2', 'B8A_2', 'QA60_2', 'LAI_2', 'NDVI_2', 'wetness_2', 'B4_contrast_2', 'B8A_maxcorr_2', 'B3_imcorr1_2']).classify(trainedClassifier)




//var classificada = stack.classify(trainedClassifier)
var bands_ano_0 =  ["B2", "B3", "B4", "B5", "B7", "B8", "B8A", "QA60", "LAI", "NDVI", "wetness", "B4_contrast", "B8A_maxcorr", "B3_imcorr1"]
var bands_ano_1 = ["B2_1", "B3_1", "B4_1", "B5_1", "B7_1", "B8_1", "B8A_1", "QA60_1", "LAI_1", "NDVI_1", "wetness_1", "B4_contrast_1", "B8A_maxcorr_1", "B3_imcorr1_1"]
var bands_ano_2 = ["B2_2", "B3_2", "B4_2", "B5_2", "B7_2", "B8_2", "B8A_2", "QA60_2", "LAI_2", "NDVI_2", "wetness_2", "B4_contrast_2", "B8A_maxcorr_2", "B3_imcorr1_2"]

/////Separando amostras
var datasetTreino = mergeAmostras.filter(ee.Filter.lt('amostra', 0.7))
var datasetTeste = mergeAmostras.filter(ee.Filter.gte('amostra', 0.7))




/////Classificador
var trainedClassifier_ano_1 = ee.Classifier.smileRandomForest({numberOfTrees: 25,
          minLeafPopulation: 6,
          maxNodes: 5}).train({
          features:datasetTreino,
          classProperty: classe,
          inputProperties: bands_ano_1
});

var trainedClassifier_ano_2 = ee.Classifier.smileRandomForest({numberOfTrees: 25,
          minLeafPopulation: 6,
          maxNodes: 5}).train({
          features:datasetTreino,
          classProperty: classe,
          inputProperties: bands_ano_2
});







////////////Visao geral das variáveis /////////////////////
print('Random Forest, explicado para ano 1', trainedClassifier_ano_1.explain());
print('Random Forest, explicado para ano 2', trainedClassifier_ano_2.explain());


// Calculando o quanto cada variável amostrada contribui através do Gini Importance
var importance = ee.Dictionary(trainedClassifier_ano_1.explain().get('importance'))
var sum = importance.values().reduce(ee.Reducer.sum())

var relativeImportance = importance.map(function(key, val) {
   return (ee.Number(val).multiply(100)).divide(sum)
  });
print(relativeImportance, 'Importancia relativa');


var importanceFc = ee.FeatureCollection([
  ee.Feature(null, relativeImportance)
]);

// Plottando resultados em gráfico
var chart2 = ui.Chart.feature.byProperty({
  features: importanceFc
}).setOptions({
      title: 'RF Importancia das variaveis',
      vAxis: {title: 'Importancia'},
      hAxis: {title: 'Bandas utilizadas'}
  });
print(chart2, 'Importancia relativa');


/////////////////////// Gerando estatísticas para medir acurácia do sistema ///////////
var testAccuracy = trainedClassifier_ano_1.confusionMatrix();
print('RF, Matriz de confusão, ano 1', testAccuracy);

//edida percentual da quantidade de amostras corretamente classificadas, em relação ao total de amostras disponíveis. 
print('RF, precisão global, ano 1', testAccuracy.accuracy());

//Kappa testa a acurácia da classificação, ou seja, qual bem ela classifica quando comparada com
//uma classificação realizada aleatoriamente
print('RF, Kappa teste, ano 1 ', testAccuracy.kappa());


//frequência com que as características reais no solo são mostradas corretamente no mapa 
//classificado ou a probabilidade de uma determinada cobertura/uso da terra ser classificada como tal no mapa.
print('RF, Precisão do produtor, ano 1', testAccuracy.producersAccuracy());


//Acurácia consumidor diz com que frequência a classe do mapa estará realmente presente no solo ou em sua referência
print('Rf, Precisão do consumidor, ano 1', testAccuracy.consumersAccuracy());


var testAccuracy_ano_2 = trainedClassifier_ano_2.confusionMatrix();
print('RF, Matriz de confusão, ano 2', testAccuracy_ano_2);

//edida percentual da quantidade de amostras corretamente classificadas, em relação ao total de amostras disponíveis.
print('RF, precisão global, ano 2', testAccuracy_ano_2.accuracy());

//Kappa testa a acurácia da classificação, ou seja, qual bem ela classifica quando comparada com
//uma classificação realizada aleatoriamente
print('RF, Kappa teste, ano 2 ', testAccuracy_ano_2.kappa());



//Subtrair a imagem s2filtered_ano_2 da imagem s2filtered_ano_1
var s2filtered_desmate = classificada_ano_1.subtract(classificada_ano_2)




//Calculando a área de desmate
//Multiply para binarizar a imagem
var area_desmate = s2filtered_desmate.multiply(ee.Image.pixelArea()).divide(10000);

var area_desmate = area_desmate.reduceRegion({
  reducer: ee.Reducer.sum(),
  geometry: area_desmate.geometry(),
  scale: 10,
  maxPixels: 1e9
});

print('Área de desmate', area_desmate);

//Calculando área total da variável area_estudo
var area_total = area_estudo.multiply(ee.Image.pixelArea()).divide(10000);
print('Área total amostrada', area_total);





// Get characteristics of the image classificada_ano_1, such as the number of pixels, maximum, minimum, and mean values.
print('classificada_ano_1', classificada_ano_1);

//Add a imagem s2filtered_desmate no mapa

Map.addLayer(s2filtered_ano_2, {min: 0, max: 1,
                            palette: ['black', 'green']},
                                            'Máscara Desmate',
                                              true, //Show
                                                0.5 // Opacity
);


Map.addLayer(s2filtered_ano_1, {min: 0, max: 1,
                            palette: ['black', 'green']},
                                            'Máscara Floresta_ano_1',
                                              true, //Show
                                                0.5 // Opacity

                                                );  


Map.addLayer(s2filtered_ano_1, {min: 0, max: 1,
                            palette: ['black', 'green']},
                                            'Máscara Floresta_ano_1',
                                              true, //Show
                                                0.5 // Opacity

                                                );  




//Configurando visualizacao de imagem sentinel-2
var visualization = {
  min: 0.0,
  max: 0.3,
  bands: ['B4', 'B3', 'B2'],
  gamma: 0.7
};
//Configurando visualizacao do RF classificado
Map.addLayer(s2filtered,
              visualization,
                'Sentinel-2, RGB',
                      true)

Map.addLayer(classificada, {min: 0, max: 3,
                            palette: ['maroon', //Cacau
                                      'blue', // ´Água
                                        'yellow', // pastagem
                                          'green' // Floresta nativa
]},                                      
                                            'Random Forest - Resultados',
                                              true, //Show
                                                0.5 // Opacity
                                                
                                                );
                                            
Map.addLayer(classificada_cart, {min: 0, max: 3,
                            palette: [
                                      'blue', // ´Água
                                        'yellow', // pastagem
                                          'green' // Floresta nativa
]},   
                                            'CART - Resultados',
                                              true, //Show
                                                0.5); // Opacity)



Map.centerObject(area_estudo, 11)

