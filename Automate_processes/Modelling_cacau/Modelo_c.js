

//Área de estudo
var area_estudo = AOI;
var limite_propriedades = Limite_lotes
var amostra_cacau_total = Amostras_cacau
var aoi_uso_solo_propriedades = aoi_uso_solo

/////////////////////// Amostragem //////////////////////////////////////////////////////
var cacau = Pol_cacau;
var sample_cacau = ee.FeatureCollection.randomPoints({region: cacau,
                                                      points: 700,
                                                      });


var sample_cacau =  sample_cacau.map(function(feature){return feature.set('Classe', 0)});



// aoi_uso_solo_propriedades is a FeatureCollection with the polygons of the properties and a column called Classe_1 with the class of the land use
//Select only the polygons which the Classe_1 is 1 (forest)
var aoi_uso_solo_propriedades = aoi_uso_solo_propriedades.filter(ee.Filter.eq('Classe_1', 1))

//Within these polygons in aoi_uso_solo_propriedades, create a dataset with 700 points]
var sample_floresta = ee.FeatureCollection.randomPoints({region: aoi_uso_solo_propriedades,
                                                      points: 700,
                                                      });

//Add a column called Classe_uso with the value 1
var sample_floresta =  sample_floresta.map(function(feature){return feature.set('Classe_uso_sample', 1)});









///////////////////// Classes de uso do solo amostradas///////////////////////////////
//Classe floresta


// Dados da copernicus com resolução de 10 metros para o ano de 2021
var dataset = ee.ImageCollection('ESA/WorldCover/v200').first();
//Get information about the dataset, including the band names
var info = dataset.getInfo();
print('Dataset information: ', info);

var forest =  dataset.select('Map').eq(10).selfMask().clip(area_estudo)

// Maskout the Pol_cacau areas from the forest
var forest = forest.updateMask(forest.mask().and(cacau.not()));


var sample_floresta = forest.stratifiedSample({numPoints: 750, 
                                      classBand: 'Map',
                                      region: area_estudo,
                                      scale: 30,
                                      geometries: true
});

var sample_floresta =  sample_floresta.map(function(feature){
  return feature.set('Classe', 1) });


var pastagem = dataset.clip(area_estudo).select('Map').eq(30).selfMask()
var sample_pastagem = pastagem.stratifiedSample({numPoints: 750, 
                                      classBand: 'Map',
                                      region: area_estudo,
                                      scale: 10,
                                      geometries: true
});


var sample_pastagem =  sample_pastagem.map(function(feature){
  return feature.set('Classe', 2) });
  
  
  
  



//Amostras
var amostras = sample_cacau.merge(sample_floresta).merge(sample_pastagem)


print('Todas as amostras, com classes diferentes', amostras)
//var pts_ndende= Ptos_n_cacau
// var dende = Pol_cacau;
// var ndende = Pol_n_cacau;

//Mesclando camadas
//var features = Pol_cacau.merge(Pol_n_cacau)

// var Amostrasdende = pts_dende.merge(pts_n_dende);
// var amostra_v01 = Ptos_cacau

//print('Amostras de poligonos', features)
// print('Amostras de pontos', Amostrasdende)
// print('Amostra anterior', amostra_v01)


//Carregar ALOS
var alos = ee.ImageCollection("JAXA/ALOS/PALSAR/YEARLY/SAR");
//Carregar Sentinel-1 TOA reflectance data/
var s1 = ee.ImageCollection("COPERNICUS/S1_GRD");
//Carregar Sentinel-2 TOA reflectance data/
var s2 = ee.ImageCollection('COPERNICUS/S2');
//Carregar Landsat 8 TOA reflectance data/
var lsat8 = ee.ImageCollection('LANDSAT/LC08/C02/T1_TOA');



///////////////////////////////////////////////// Funções inserção de variaveis ////////////////////////////////////
//Função para rodar o filtro lee (ALOS 2 -PALSAR-2)
function filterleealos(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Homogenius Class - Enhanced Lee Filter)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLeealos =['HHLee','HVLee']
  var leefiltersalos = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefiltersalos.rename(BandsLeealos));
}

//Função para rodar o filtro lee (Sentinel 1)
function filterlee(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Classe Homogênea - Filtro Lee Melhorado)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLee =['VVLee','VHLee']
  var leefilters = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefilters.rename(BandsLee));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function addTasseledCap (image) {
  var b = image.select("B2", "B3", "B4", "B5", "B6", "B7");
  var brightness_coefficents= ee.Image([0.3029, 0.2786, 0.4733, 0.5599, 0.508, 0.1872])
  var greenness_coefficents= ee.Image([-0.2941, -0.243, -0.5424, 0.7276, 0.0713, -0.1608]);
  var wetness_coefficents= ee.Image([0.1511, 0.1973, 0.3283, 0.3407, -0.7117, -0.4559]);
  
  
  var brightness = image.expression(
          '(B * BRIGHTNESS)',
          {
              'B':b,
              'BRIGHTNESS': brightness_coefficents
              }
          );
  var greenness = image.expression(
  '(B * GREENNESS)',
          {
              'B':b,
              'GREENNESS': greenness_coefficents
              }
          );
  var wetness = image.expression(
  '(B * WETNESS)',
          {
              'B':b,
              'WETNESS': wetness_coefficents
              }
          );
  

brightness = brightness.reduce(ee.call("Reducer.sum"));
greenness = greenness.reduce(ee.call("Reducer.sum"));
wetness = wetness.reduce(ee.call("Reducer.sum"));
//Add the resulting bands to the image while renaming them.
image = image.addBands(brightness.rename('brightness'));
image = image.addBands(greenness.rename('greenness'));
image = image.addBands(wetness.rename('wetness'));
return image;
  
}


// var start_date = "2018-12-30"
// var end_date = "2018-12-31"
// var cloud_cover = 10

// var select_2018 = ee.Image("LANDSAT/LC08/C01/T1_RT_TOA/LC08_217073_20180613");
// var BRD = boxes.geometry();
// Map.centerObject(BRD);

// var landsat8_collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_TOA')
//       .filterDate('2013-09-01', '2013-12-31')
//       .filterMetadata('CLOUD_COVER', 'less_than', cloud_cover)
//       .filterBounds(BRD)


var landsat8_tasseled_cap = landsat8_collection.map(calculateTasseledCap);

// reduce tasseled cap collection to an image so we can use .reduceRegion()
var tasseledcapImg = landsat8_tasseled_cap.mosaic()

var reducer = ee.Reducer.mean() // change to whichever reducer you would like to use

// map over each feature in the city collection
var city_stats = boxes.map(function(feature){
// reduce the feature to get band statistics
var zonalStats = tasseledcapImg.reduceRegion({
  reducer: reducer,
  geometry:feature.geometry(),
  scale:30
})
// return same feature but with the zonal stat results
return feature.set(zonalStats)
})

print(city_stats)

Map.addLayer(landsat8_tasseled_cap,{},'Landsat 8 Tasseled Cap');
Map.addLayer(landsat8_tasseled_cap,{min: 0, max:1, bands:['brightness']},'brightness');
Map.addLayer(landsat8_tasseled_cap,{min: 0, max:1, bands:['greenness']},'greenness');
Map.addLayer(landsat8_tasseled_cap,{min: 0, max:1, bands:['wetness']},'wetness');
Map.addLayer(boxes,{color:'yellow'},'Cities');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Função para calcular o NDVI a partir do Sentinel 2
function addNDVIs2(image) {
  var ndvi = image.normalizedDifference(['B8', 'B4']).rename('NDVI');
  return image.addBands(ndvi);
}





//Função para Adicionar o RVI (ALOS 2-PALSAR-2)
function addalosRVI(image)
{
  var rvialos = image.expression(
  '4 * hv / (hv + hh)',
  {
     hh: image.select('HHLee'),    
     hv: image.select('HVLee')
  });
  return image.addBands(rvialos.rename('RVIAlos'));
}
 
//Função para Adicionar o RVI (Sentinel 1)
function addS1RVI(image)
{
  var rvi = image.expression(
  '4 * vh / (vh + vv)',
  {
     vv: image.select('VVLee'),    
     vh: image.select('VHLee')
  });
  return image.addBands(rvi.rename('RVI'));
}


//Função para Adicionar o NDVI do Landsat 8
function addLandsatNDVI(image)
{
  var Landsat_NDVI = image.expression(
    '((ir-red)/(ir+red))',
    {
      ir: image.select('B5'),
      red: image.select('B4')
    });
    return image.addBands(Landsat_NDVI.rename('LS8_NDVI'));
}


//Função para calcular e inseri LAI (Leaf Area Index)
function addLAI(image)
{
    var ndvi = image.expression(
    '((red - ir) / (red + ir))',
    {
        red: image.select('B8'),    
        ir: image.select('B4')});
        
    var LAI = ndvi.expression('0.57 * exp(2.33 * b(0))').rename('LAI');

    image = image.addBands(LAI.rename('LAI'));
return image;
}


//Function to Run and add a GLCM algorithm over image
function addGLCM(image)
{
  var glcm = image.select('B8').glcmTexture({size: 3});
  var glcmBands = glcm.select('B8_contrast');
  return image.addBands(glcmBands);
}



// Criação das máscara de nuvem usando Sentinel-2 QA60 band.
function maskS2clouds(image) {
  var qa = image.select('QA60');

// Bits 10 e 11 são nuvens e cirrus, respectivamente.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;
  
// Ambos os sinalizadores devem ser definidos como zero, indicando condições claras.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
             qa.bitwiseAnd(cirrusBitMask).eq(0));
  return image.updateMask(mask).divide(10000);
}


//Adicionando indices de vegetação
function addIndices(image){
  return image.addBands([
    image.select('B8','B4').normalizedDifference().multiply(10000).add(10000).rename('NDVI')]);
}

                     
// Fazer o empilhamento de imagens (raster stacking)
var alospalsar =  alos.filterDate('2017-06-01', '2018-10-31')
                  .select(['HH', 'HV'])
                  .map(filterleealos)
                  .map(addalosRVI);

var landsat8 =  lsat8.filterDate('2022-06-01', '2022-10-31')
                  .filterMetadata('CLOUD_COVER','less_than', 15)//25 -> 15
                  .select(['B4', 'B5'])
                  .map(addLandsatNDVI);

var sentinel1 =  s1.filterDate('2022-06-01', '2022-10-31')
                  .select(['VV', 'VH'])
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VV'))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
                  .filter(ee.Filter.eq('instrumentMode', 'IW'))
                  .filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
                  .map(filterlee)
                  .map(addS1RVI);
                  
var sentinel2 = s2.filterDate('2022-06-01', '2022-10-31')
                  .filterMetadata('CLOUDY_PIXEL_PERCENTAGE','less_than', 10)  
                  .select(['B1', 'B2', 'B3', 'B4', 'B5', 'B6','B7','B8', 'B8A', 'B11', 'B12','QA60'])
                  .map(maskS2clouds)
                  .map(addIndices)
                  .map(addLAI)
                  .map(addGLCM)
                  .map(addTasseledCap);

// Composição de um conjunto de dados na área de estudo
var landsat8 = landsat8.median().clip(area_estudo);
var s1lee = sentinel1.median().clip(area_estudo);
var s2filtered = sentinel2.median().clip(area_estudo);
var aloslee = alospalsar.median().clip(area_estudo);
var stack = s2filtered.addBands(landsat8).addBands(s1lee).addBands(aloslee);

// Mostrar o stack de imagens
Map.addLayer(stack, {bands: ['B11', 'B8', 'B4'], min: 0, max: 0.3}, 'stack', false);
//Map.addLayer(al2_composite, {bands: ['HH', 'HV'], min: 0, max: 8500}, 'ALOS2 composite', false);

var bands = ['B1', 'B2', 'B3','B4', 'B5', 'B6','B7','B8','B8A', 'B11', 'B12','LAI','LS8_NDVI','VV','VH','RVI','HH','HV','RVIAlos'];
var classe = 'Classe';
var presence = 'Map';



// On the Sentinel-2 image, run a Tasseled cap analysis
var tasseledCap = stack.select(bands).tasseledCap({ coefficients: tcParams });
var tasseledCap = tasseledCap.addBands(stack.select(bands));
var tasseledCap = tasseledCap.addBands(stack.select(classe));
var tasseledCap = tasseledCap.addBands(stack.select(presence));




//Add the texture and tasseled cap bands to the image
var stack = stack.addBands(texture);
var stack = stack.addBands(tasseledCap);




////////////////////////////////////////// Amostras //////////////////////////////////////////////////////////
var training = stack.select(bands).sampleRegions({
  collection: amostras,
  properties: [classe],
  scale: 50
});



// var sample  = stack.select(bands).sampleRegions({
//   collection: features,
//   properties: [classe],
//     scale: 50,
//   geometries: true })

var withRandom = training.randomColumn('random');

var split =  0.7
var trainingPartition = withRandom.filter(ee.Filter.lt('random', split));
var testingPartition = withRandom.filter(ee.Filter.gte('random', split));


// //////////////////////////////////////// Treinamentos dos Classificadores //////////////////////////////////////////

// // Treinando com 70% das amostras


//RandomForest(RF)
var trainedClassifier = ee.Classifier.smileRandomForest(100).train({
    features:trainingPartition,
    classProperty: classe,
  inputProperties: bands
});




//Treinando o Classificador (Classification and Regression Trees - CART)
var trained = ee.Classifier.smileCart().train(trainingPartition, classe, bands);




//Performing a producers accuracy assessment
var testAccuracy = trainedClassifier.confusionMatrix();
print('RF, Confusion Matrix', testAccuracy);
print('RF, Overall Accuracy', testAccuracy.accuracy());
print('RF, Kappa', testAccuracy.kappa());
print('RF, Producer Accuracy', testAccuracy.producersAccuracy());
print('RF, User Accuracy', testAccuracy.usersAccuracy());
print('Rf, Consumers Accuracy', testAccuracy.consumersAccuracy());
// Performing a Allocation Disagreement test







// // // Retirando MaxEnt
// // // Definir e treinar o classificador Maxent de uma imagem por amostragens de pontos.
// // //var classifier = ee.Classifier.amnhMaxent().train({
// //   //features: trainingmaxent,
// //   //classProperty: 'presence',
// //   //inputProperties: stack.bandNames()
// // //});



// // ////////////////////////////////////////////// Imprimir os resultados /////////////////////////////////////////////

// // //Imprima algumas informações sobre o RF
print('Random Forest, explicado', trainedClassifier.explain());

// // // //Imprima algumas informações sobre o CART
// // // print('CART, explicado', trained.explain());

// // //////////////////////////////////////////// Classificadores //////////////////////////////////////////////////////

// // //Classifique o composto (Random Forest)
var classificado = stack.classify(trainedClassifier);

// //Classifique o composto (CART)
// var classificado_cart = stack.select(bands).classify(trained);


// Classifica o teste na Coleção de Feições (Random Forest)
var test = testingPartition.classify(trainedClassifier);

// // Classifica o teste na Coleção de Feições (CART)
// var testCart = testingPartition.classify(trained);

// ////////////////////////////////////////// Visualizar Camadas ///////////////////////////////////////////////////

// // Adicionando Camadas 
Map.centerObject(area_estudo,8);
//Add area_estudo polygon to the map without any fill, only the outline.

Map.addLayer(area_estudo, {color: 'red', 
                            fillColor: null,

}
  
  
  
  , 'Area de Estudo', false);

Map.addLayer(area_estudo)



Map.addLayer(classificado, {min: 0, max: 3, palette: ['brown', 'yellow', 'grey', 'green']}, 'Random Forest', false);
//Insert legend for the classification result from Random Forest, for four classes of land cover.
var legend = ui.Panel({
  style: {
    position: 'bottom-left',  
    padding: '8px 15px'
  }
});

var legendTitle = ui.Label({

  value: 'Random Forest',
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '0'
  }
});


var makeRow = function(color, name) {
  var colorBox = ui.Label({
    style: {
      backgroundColor: '#' + color,
      // Use padding to give the box height and width.
      padding: '8px',
      margin: '0 0 4px 0'
    }
  });


  var description = ui.Label({
    value: name,
    style: {margin: '0 0 4px 6px'}
  });

  return ui.Panel({
    widgets: [colorBox, description],
    layout: ui.Panel.Layout.Flow('horizontal')
  });

};

var legendCacao = makeRow('brown', 'Cacau');  
var legendNaoCacao = makeRow('yellow', 'Não Cacau');
var legendNaoClassificado = makeRow('grey', 'Não Classificado');
var legendAgua = makeRow('green', 'Água');

legend.add(legendTitle);
legend.add(legendCacao);
legend.add(legendNaoCacao);
legend.add(legendNaoClassificado);
legend.add(legendAgua);


Map.add(legend);


















// Map.addLayer(limite_propriedades, {color: 'red'}, 'Limite das propriedades');
// Map.addLayer(pts_dende, {color: 'brown'}, 'Pontos amostras cacau');
// Map.addLayer(pts_n_dende, {color: 'red'}, 'Pontos amostras não cacau');
// Map.addLayer(amostra_cacau_total, {color: 'brown'}, 'Pontos amostras cacau todas amostras');

// From the stack variable, select the tasseled cap bands
var tasseledCap = stack.select(bands);
//and use addLayer to display the image
Map.addLayer(tasseledCap, {bands: ['brightness', 'greenness', 'wetness'], min: -0.2, max: 0.4}, 'Tasseled Cap');


// //////////////////////////////////////////Filtro Majoritário na Classificação////////////////////////////

// // CART
// var kernel = ee.Kernel.square(2);
// var fmajoritclasscart = classificado_cart.reduceNeighborhood({
//   reducer:ee.Reducer.mode(),
//   kernel:kernel,
// });



// // Random Forest
// var fmajoritclassrf = classificado.reduceNeighborhood({
//   reducer:ee.Reducer.mode(),
//   kernel:kernel,
// });

// //Adicionando a camada
// //Map.addLayer(fmajoritclasscart,{min: 1, max: 2, palette: ['black', 'green']},'Majoritario_CART');
// //Map.addLayer(fmajoritclassrf,{min: 1, max: 2, palette: ['black', 'green']},'Majoritario_RF');


// //////////////////////////////////////////// Validação /////////////////////////////////////////////////////
// //Random Forest
// var MatrizErro = test.errorMatrix(classe, 'classification');
// var AG = MatrizErro.accuracy()
// var Kappa = MatrizErro.kappa()
// var info = AG.getInfo()
// print('Acurácia geral da validação - RF', AG)
// print('Kappa - RF',Kappa)
// print('Info', info)


// //Classification and Regression Trees
// var MatrizErroCart = testCart.errorMatrix(classe, 'classification');
// var AG = MatrizErroCart.accuracy()
// var Kappa = MatrizErroCart.kappa()

// print('Acurácia geral da validação - CART', AG)
// print('Kappa - CART',Kappa)

// ///////////////////////////////////////Visualização dos Modelos////////////////////////////////////////////

// //Selecionar as imagens 
// var images=[
//   ee.ImageCollection(classificado),
//   ee.ImageCollection(classificado_cart),
//   //ee.ImageCollection(zones),
//   ee.ImageCollection(stack)
//   ];

// var vis = [
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 0, max: 0.3, bands: ['B4', 'B3', 'B2']}
// ];

// var NAMES = [
//   'Random Forest',
//   'CART',
//   //'MaxEnt',
//   'Imagens'
// ];

// // Cria um mapa para cada opção de visualização.
// var maps = [];
// NAMES.forEach(function(name,index) {
//   var map = ui.Map();
//   map.add(ui.Label(name));
//   map.addLayer(images[index], vis[index], name);
//   map.setControlVisibility(true);
//   maps.push(map);
// });

// var linker = ui.Map.Linker(maps);
// // Habilita o Zoom no mapa da esquerda "Antes".
// maps[0].setControlVisibility({zoomControl: true});
// // Mostra a escala (por exemplo, 500m) no mapa da direita.
// maps[0].setControlVisibility({scaleControl: true});
// maps[1].setControlVisibility({scaleControl: true});
// maps[2].setControlVisibility({scaleControl: true});
// //maps[3].setControlVisibility({scaleControl: true});

// // Cria o título.
// var title = ui.Label('Modelos - Cacau', {
//   stretch: 'horizontal',
//   textAlign: 'center',
//   fontWeight: 'bold',
//   fontSize: '24px',
//   color: '#0000FF'
// });

// // Cria um painel para armazenar a interface gráfica.
// // var panel = ui.Panel();
// // panel.style().set('width', '300px');

// // Cria uma grade de mapas.
// // var mapGrid = ui.Panel([
// //     ui.Panel([maps[0], maps[1]], null, {stretch: 'both'}),
// //     ui.Panel(maps[2], null, {strech: 'vertical'})
// //   ],
// //   ui.Panel.Layout.Flow('horizontal'), {stretch: 'both'}
// // );

// //Imagens: Layer error: Image.visualize: Cannot provide a palette when visualizing more than one band.
// // Adiciona os mapas e títulos na "ui.root".
// // ui.root.widgets().reset([title, mapGrid]);
// // ui.root.setLayout(ui.Panel.Layout.Flow('vertical'));

// // // Centraliza os mapas.
// // maps[0].setCenter(-53.7296, -4.553,10);

// // ui.Map.Linker([maps[0], maps[1], maps[2]]);

// //////////////////////////////////////////// Exportando resultados das Classificações //////////////////////////////////
// //CART
// Export.image.toDrive({
//   image:fmajoritclasscart,
//   description:'Cacau_CART_sol',
//   scale:10,
//   folder:'GEE_export',
//   region:area_estudo,
//   maxPixels:1e13
// });

// // MaxEnt
// // Export.image.toDrive({
// //   image:fmajoritclassmax,
// //   description:'Cacau_MaxEnt_sol',
// //   scale:10,
// //   folder:'GEE_export',
// //   region:area_estudo,
// //   maxPixels:1e13
// // });

// //Random forest 
// Export.image.toDrive({
//   image:fmajoritclassrf,
//   description:'Cacau_RF_sol',
//   scale:10,
//   folder:'GEE_export',
//   region:area_estudo,
//   maxPixels:1e13
// });

// //Amostras
// Export.table.toDrive({
//   collection: dende,
//   description:'Cacau_Amostras_pol_sol',
//   folder: 'GEE_export',
//   fileFormat: 'KML'
// });


