__all__ = ['Modelo_classificacao_cacau_py']

# Don't look below, you will not understand this Python code :) I don't.

from js2py.pyjs import *
# setting scope
var = Scope( JS_BUILTINS )
set_global_object(var)

# Code follows:
var.registers(['cacau', 'training', 's1', 'amostra_cacau_total', 'sample_cacau', 'MatrizErro', 'vis', 'alos', 'addalosRVI', 'sentinel2', 'testingPartition', 'relativeImportance', 'addIndices', 'test', 'chart2', 'sample_floresta', 'addS1RVI', 'dataset', 'addLAI', 'split', 'withRandom', 'sum', 'lsat8', 'classe', 'filterlee', 'trained', 'presence', 'sample_pastagem', 'importanceFc', 'amostras', 'trainedClassifier', 'sentinel1', 'AG', 'filterleealos', 'stack', 'limite_propriedades', 'pastagem', 'forest', 'alospalsar', 'bands', 'importance', 'area_estudo', 's2filtered', 'landsat8', 'classificado', 'trainingPartition', 'Kappa', 'todas_classes', 's1lee', 'maskS2clouds', 'aloslee', 's2', 'addLandsatNDVI'])
@Js
def PyJsHoisted_filterleealos_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['leefiltersalos', 'image', 'BandsLeealos', 'kernellee'])
    var.put('kernellee', var.get('ee').get('Kernel').callprop('square', Js(3.0), Js('pixels')))
    var.put('BandsLeealos', Js([Js('HHLee'), Js('HVLee')]))
    var.put('leefiltersalos', var.get('image').callprop('reduceNeighborhood', Js({'reducer':var.get('ee').get('Reducer').callprop('mean'),'kernel':var.get('kernellee')})))
    return var.get('image').callprop('addBands', var.get('leefiltersalos').callprop('rename', var.get('BandsLeealos')))
PyJsHoisted_filterleealos_.func_name = 'filterleealos'
var.put('filterleealos', PyJsHoisted_filterleealos_)
@Js
def PyJsHoisted_filterlee_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['image', 'BandsLee', 'leefilters', 'kernellee'])
    var.put('kernellee', var.get('ee').get('Kernel').callprop('square', Js(3.0), Js('pixels')))
    var.put('BandsLee', Js([Js('VVLee'), Js('VHLee')]))
    var.put('leefilters', var.get('image').callprop('reduceNeighborhood', Js({'reducer':var.get('ee').get('Reducer').callprop('mean'),'kernel':var.get('kernellee')})))
    return var.get('image').callprop('addBands', var.get('leefilters').callprop('rename', var.get('BandsLee')))
PyJsHoisted_filterlee_.func_name = 'filterlee'
var.put('filterlee', PyJsHoisted_filterlee_)
@Js
def PyJsHoisted_addalosRVI_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['rvialos', 'image'])
    var.put('rvialos', var.get('image').callprop('expression', Js('4 * hv / (hv + hh)'), Js({'hh':var.get('image').callprop('select', Js('HHLee')),'hv':var.get('image').callprop('select', Js('HVLee'))})))
    return var.get('image').callprop('addBands', var.get('rvialos').callprop('rename', Js('RVIAlos')))
PyJsHoisted_addalosRVI_.func_name = 'addalosRVI'
var.put('addalosRVI', PyJsHoisted_addalosRVI_)
@Js
def PyJsHoisted_addS1RVI_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['rvi', 'image'])
    var.put('rvi', var.get('image').callprop('expression', Js('4 * vh / (vh + vv)'), Js({'vv':var.get('image').callprop('select', Js('VVLee')),'vh':var.get('image').callprop('select', Js('VHLee'))})))
    return var.get('image').callprop('addBands', var.get('rvi').callprop('rename', Js('RVI')))
PyJsHoisted_addS1RVI_.func_name = 'addS1RVI'
var.put('addS1RVI', PyJsHoisted_addS1RVI_)
@Js
def PyJsHoisted_addLandsatNDVI_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['Landsat_NDVI', 'image'])
    var.put('Landsat_NDVI', var.get('image').callprop('expression', Js('((ir-red)/(ir+red))'), Js({'ir':var.get('image').callprop('select', Js('B5')),'red':var.get('image').callprop('select', Js('B4'))})))
    return var.get('image').callprop('addBands', var.get('Landsat_NDVI').callprop('rename', Js('LS8_NDVI')))
PyJsHoisted_addLandsatNDVI_.func_name = 'addLandsatNDVI'
var.put('addLandsatNDVI', PyJsHoisted_addLandsatNDVI_)
@Js
def PyJsHoisted_addLAI_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['image', 'ndvi', 'LAI'])
    var.put('ndvi', var.get('image').callprop('expression', Js('((red - ir) / (red + ir))'), Js({'red':var.get('image').callprop('select', Js('B8')),'ir':var.get('image').callprop('select', Js('B4'))})))
    var.put('LAI', var.get('ndvi').callprop('expression', Js('0.57 * exp(2.33 * b(0))')).callprop('rename', Js('LAI')))
    var.put('image', var.get('image').callprop('addBands', var.get('LAI').callprop('rename', Js('LAI'))))
    return var.get('image')
PyJsHoisted_addLAI_.func_name = 'addLAI'
var.put('addLAI', PyJsHoisted_addLAI_)
@Js
def PyJsHoisted_maskS2clouds_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['mask', 'image', 'qa', 'cloudBitMask', 'cirrusBitMask'])
    var.put('qa', var.get('image').callprop('select', Js('QA60')))
    var.put('cloudBitMask', (Js(1.0)<<Js(10.0)))
    var.put('cirrusBitMask', (Js(1.0)<<Js(11.0)))
    var.put('mask', var.get('qa').callprop('bitwiseAnd', var.get('cloudBitMask')).callprop('eq', Js(0.0)).callprop('and', var.get('qa').callprop('bitwiseAnd', var.get('cirrusBitMask')).callprop('eq', Js(0.0))))
    return var.get('image').callprop('updateMask', var.get('mask')).callprop('divide', Js(10000.0))
PyJsHoisted_maskS2clouds_.func_name = 'maskS2clouds'
var.put('maskS2clouds', PyJsHoisted_maskS2clouds_)
@Js
def PyJsHoisted_addIndices_(image, this, arguments, var=var):
    var = Scope({'image':image, 'this':this, 'arguments':arguments}, var)
    var.registers(['image'])
    return var.get('image').callprop('addBands', Js([var.get('image').callprop('select', Js('B8'), Js('B4')).callprop('normalizedDifference').callprop('multiply', Js(10000.0)).callprop('add', Js(10000.0)).callprop('rename', Js('NDVI'))]))
PyJsHoisted_addIndices_.func_name = 'addIndices'
var.put('addIndices', PyJsHoisted_addIndices_)
var.put('area_estudo', var.get('AOI'))
var.put('limite_propriedades', var.get('Limite_lotes'))
var.put('amostra_cacau_total', var.get('Amostras_cacau'))
var.put('cacau', var.get('Pol_cacau'))
var.put('sample_cacau', var.get('ee').get('FeatureCollection').callprop('randomPoints', Js({'region':var.get('cacau'),'points':Js(200.0)})))
@Js
def PyJs_anonymous_0_(feature, this, arguments, var=var):
    var = Scope({'feature':feature, 'this':this, 'arguments':arguments}, var)
    var.registers(['feature'])
    return var.get('feature').callprop('set', Js('Classe'), Js(0.0))
PyJs_anonymous_0_._set_name('anonymous')
var.put('sample_cacau', var.get('sample_cacau').callprop('map', PyJs_anonymous_0_))
var.put('dataset', var.get('ee').callprop('ImageCollection', Js('ESA/WorldCover/v200')).callprop('first'))
var.put('todas_classes', var.get('dataset').callprop('clip', var.get('area_estudo')))
var.put('forest', var.get('dataset').callprop('select', Js('Map')).callprop('eq', Js(10.0)).callprop('selfMask').callprop('clip', var.get('area_estudo')))
var.put('sample_floresta', var.get('forest').callprop('stratifiedSample', Js({'numPoints':Js(300.0),'classBand':Js('Map'),'region':var.get('area_estudo'),'scale':Js(10.0),'geometries':Js(True)})))
@Js
def PyJs_anonymous_1_(feature, this, arguments, var=var):
    var = Scope({'feature':feature, 'this':this, 'arguments':arguments}, var)
    var.registers(['feature'])
    return var.get('feature').callprop('set', Js('Classe'), Js(1.0))
PyJs_anonymous_1_._set_name('anonymous')
var.put('sample_floresta', var.get('sample_floresta').callprop('map', PyJs_anonymous_1_))
var.put('pastagem', var.get('dataset').callprop('clip', var.get('area_estudo')).callprop('select', Js('Map')).callprop('eq', Js(30.0)).callprop('selfMask'))
var.put('sample_pastagem', var.get('pastagem').callprop('stratifiedSample', Js({'numPoints':Js(350.0),'classBand':Js('Map'),'region':var.get('area_estudo'),'scale':Js(10.0),'geometries':Js(True)})))
@Js
def PyJs_anonymous_2_(feature, this, arguments, var=var):
    var = Scope({'feature':feature, 'this':this, 'arguments':arguments}, var)
    var.registers(['feature'])
    return var.get('feature').callprop('set', Js('Classe'), Js(2.0))
PyJs_anonymous_2_._set_name('anonymous')
var.put('sample_pastagem', var.get('sample_pastagem').callprop('map', PyJs_anonymous_2_))
var.put('amostras', var.get('sample_cacau').callprop('merge', var.get('sample_floresta')).callprop('merge', var.get('sample_pastagem')))
var.get('print')(Js('Todas as amostras, com classes diferentes'), var.get('amostras'))
var.put('alos', var.get('ee').callprop('ImageCollection', Js('JAXA/ALOS/PALSAR/YEARLY/SAR')))
var.put('s1', var.get('ee').callprop('ImageCollection', Js('COPERNICUS/S1_GRD')))
var.put('s2', var.get('ee').callprop('ImageCollection', Js('COPERNICUS/S2')))
var.put('lsat8', var.get('ee').callprop('ImageCollection', Js('LANDSAT/LC08/C02/T1_TOA')))
pass
pass
pass
pass
pass
pass
pass
pass
var.put('alospalsar', var.get('alos').callprop('filterDate', Js('2017-06-01'), Js('2018-10-31')).callprop('select', Js([Js('HH'), Js('HV')])).callprop('map', var.get('filterleealos')).callprop('map', var.get('addalosRVI')))
var.put('landsat8', var.get('lsat8').callprop('filterDate', Js('2022-06-01'), Js('2022-10-31')).callprop('filterMetadata', Js('CLOUD_COVER'), Js('less_than'), Js(15.0)).callprop('select', Js([Js('B4'), Js('B5')])).callprop('map', var.get('addLandsatNDVI')))
def PyJs_LONG_3_(var=var):
    return var.get('s1').callprop('filterDate', Js('2022-06-01'), Js('2022-10-31')).callprop('select', Js([Js('VV'), Js('VH')])).callprop('filter', var.get('ee').get('Filter').callprop('listContains', Js('transmitterReceiverPolarisation'), Js('VV'))).callprop('filter', var.get('ee').get('Filter').callprop('listContains', Js('transmitterReceiverPolarisation'), Js('VH'))).callprop('filter', var.get('ee').get('Filter').callprop('eq', Js('instrumentMode'), Js('IW')))
var.put('sentinel1', PyJs_LONG_3_().callprop('filter', var.get('ee').get('Filter').callprop('eq', Js('orbitProperties_pass'), Js('DESCENDING'))).callprop('map', var.get('filterlee')).callprop('map', var.get('addS1RVI')))
def PyJs_LONG_4_(var=var):
    return var.get('s2').callprop('filterDate', Js('2022-06-01'), Js('2022-10-31')).callprop('filterMetadata', Js('CLOUDY_PIXEL_PERCENTAGE'), Js('less_than'), Js(10.0)).callprop('select', Js([Js('B1'), Js('B2'), Js('B3'), Js('B4'), Js('B5'), Js('B6'), Js('B7'), Js('B8'), Js('B8A'), Js('B11'), Js('B12'), Js('QA60')])).callprop('map', var.get('maskS2clouds')).callprop('map', var.get('addIndices')).callprop('map', var.get('addLAI'))
var.put('sentinel2', PyJs_LONG_4_())
var.put('landsat8', var.get('landsat8').callprop('median').callprop('clip', var.get('area_estudo')))
var.put('s1lee', var.get('sentinel1').callprop('median').callprop('clip', var.get('area_estudo')))
var.put('s2filtered', var.get('sentinel2').callprop('median').callprop('clip', var.get('area_estudo')))
var.put('aloslee', var.get('alospalsar').callprop('median').callprop('clip', var.get('area_estudo')))
var.put('stack', var.get('s2filtered').callprop('addBands', var.get('landsat8')).callprop('addBands', var.get('s1lee')).callprop('addBands', var.get('aloslee')))
var.get('Map').callprop('addLayer', var.get('stack'), Js({'bands':Js([Js('B11'), Js('B8'), Js('B4')]),'min':Js(0.0),'max':Js(0.3)}), Js('stack'), Js(False))
var.put('bands', Js([Js('B1'), Js('B2'), Js('B3'), Js('B4'), Js('B5'), Js('B6'), Js('B7'), Js('B8'), Js('B8A'), Js('B11'), Js('B12'), Js('LAI'), Js('LS8_NDVI'), Js('VV'), Js('VH'), Js('RVI'), Js('HH'), Js('HV'), Js('RVIAlos')]))
var.put('classe', Js('Classe'))
var.put('presence', Js('Map'))
var.put('training', var.get('stack').callprop('select', var.get('bands')).callprop('sampleRegions', Js({'collection':var.get('amostras'),'properties':Js([var.get('classe')]),'scale':Js(50.0)})))
var.put('withRandom', var.get('training').callprop('randomColumn', Js('random')))
var.put('split', Js(0.9))
var.put('trainingPartition', var.get('withRandom').callprop('filter', var.get('ee').get('Filter').callprop('lt', Js('random'), var.get('split'))))
var.put('testingPartition', var.get('withRandom').callprop('filter', var.get('ee').get('Filter').callprop('gte', Js('random'), var.get('split'))))
var.put('trainedClassifier', var.get('ee').get('Classifier').callprop('smileRandomForest', Js({'numberOfTrees':Js(6.0),'minLeafPopulation':Js(6.0),'maxNodes':Js(5.0)})).callprop('train', Js({'features':var.get('trainingPartition'),'classProperty':var.get('classe'),'inputProperties':var.get('bands')})))
var.put('trained', var.get('ee').get('Classifier').callprop('smileCart').callprop('train', var.get('trainingPartition'), var.get('classe'), var.get('bands')))
var.get('print')(Js('Random Forest, explicado'), var.get('trainedClassifier').callprop('explain'))
var.put('importance', var.get('ee').callprop('Dictionary', var.get('trainedClassifier').callprop('explain').callprop('get', Js('importance'))))
var.put('sum', var.get('importance').callprop('values').callprop('reduce', var.get('ee').get('Reducer').callprop('sum')))
@Js
def PyJs_anonymous_5_(key, val, this, arguments, var=var):
    var = Scope({'key':key, 'val':val, 'this':this, 'arguments':arguments}, var)
    var.registers(['val', 'key'])
    return var.get('ee').callprop('Number', var.get('val')).callprop('multiply', Js(100.0)).callprop('divide', var.get('sum'))
PyJs_anonymous_5_._set_name('anonymous')
var.put('relativeImportance', var.get('importance').callprop('map', PyJs_anonymous_5_))
var.get('print')(var.get('relativeImportance'), Js('Relative Importance'))
var.put('importanceFc', var.get('ee').callprop('FeatureCollection', Js([var.get('ee').callprop('Feature', var.get(u"null"), var.get('relativeImportance'))])))
var.put('chart2', var.get('ui').get('Chart').get('feature').callprop('byProperty', Js({'features':var.get('importanceFc')})).callprop('setOptions', Js({'title':Js('RF Variable Importance - Method 2'),'vAxis':Js({'title':Js('Importance')}),'hAxis':Js({'title':Js('Bands')})})))
var.get('print')(var.get('chart2'), Js('Relative Importance'))
var.put('classificado', var.get('stack').callprop('classify', var.get('trainedClassifier')))
var.put('test', var.get('testingPartition').callprop('classify', var.get('trainedClassifier')))
var.put('vis', Js({'bands':Js([Js('Map')])}))
var.get('Map').callprop('centerObject', var.get('area_estudo'), Js(8.0))
var.get('Map').callprop('addLayer', var.get('classificado'), Js({'min':Js(1.0),'max':Js(2.0),'palette':Js([Js('grey'), Js('blue')])}), Js('Random Forest'))
var.get('Map').callprop('addLayer', var.get('sample_cacau'), Js({'color':Js('brown')}), Js('Pontos amostras cacau'))
var.get('Map').callprop('addLayer', var.get('sample_pastagem'), Js({'color':Js('yellow')}), Js('Pontos amostras de pastagem'))
var.get('Map').callprop('addLayer', var.get('sample_floresta'), Js({'color':Js('purple')}), Js('Pontos amostras de floresta'))
var.get('Map').callprop('addLayer', var.get('limite_propriedades'), Js({'color':Js('blue')}), Js('Limites das propriedades rurais'))
var.get('Map').callprop('addLayer', var.get('cacau'), Js({'color':Js('brown')}), Js('Polígonos de cacau sombreado'))
var.put('MatrizErro', var.get('test').callprop('errorMatrix', var.get('classe'), Js('classification')))
var.put('AG', var.get('MatrizErro').callprop('accuracy'))
var.put('Kappa', var.get('MatrizErro').callprop('kappa'))
var.get('print')(Js('Acurácia geral da validação - RF'), var.get('AG'))
var.get('print')(Js('Kappa - RF'), var.get('Kappa'))
pass


# Add lib to the module scope
Modelo_classificacao_cacau_py = var.to_python()