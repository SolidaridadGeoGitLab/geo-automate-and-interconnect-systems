

//Área de estudo
var area_estudo = AOI;
var limite_propriedades = Limite_lotes
var amostra_cacau_total = Amostras_cacau



/////////////////////// Amostragem  CACAU //////////////////////////////////////////////////////
var cacau = Pol_cacau;
var sample_cacau = ee.FeatureCollection.randomPoints({region: cacau,
                                                      points: 450,
                                                      });


var sample_cacau =  sample_cacau.map(function(feature){return feature.set('Classe', 0)});


///////////////////// Classes de uso do solo amostradas///////////////////////////////
//Classe floresta


// Dados da copernicus com resolução de 10 metros para o ano de 2022
var dataset = ee.ImageCollection('ESA/WorldCover/v200').first();

var todas_classes = dataset.clip(area_estudo); 

var forest =  dataset.select('Map').eq(10).selfMask().clip(area_estudo)

var sample_floresta = forest.stratifiedSample({numPoints: 450, 
                                      classBand: 'Map',
                                      region: area_estudo,
                                      scale: 30,
                                      geometries: true
});

var sample_floresta =  sample_floresta.map(function(feature){
  return feature.set('Classe', 1) });


var pastagem = dataset.clip(area_estudo).select('Map').eq(30).selfMask()
var sample_pastagem = pastagem.stratifiedSample({numPoints: 750, 
                                      classBand: 'Map',
                                      region: area_estudo,
                                      scale: 30,
                                      geometries: true
});


var sample_pastagem =  sample_pastagem.map(function(feature){
  return feature.set('Classe', 2) });
  
//Amostras
var amostras = sample_cacau.merge(sample_floresta).merge(sample_pastagem)


print('Todas as amostras, com classes diferentes', amostras)
//var pts_ndende= Ptos_n_cacau
// var dende = Pol_cacau;
// var ndende = Pol_n_cacau;

//Mesclando camadas
//var features = Pol_cacau.merge(Pol_n_cacau)

// var Amostrasdende = pts_dende.merge(pts_n_dende);
// var amostra_v01 = Ptos_cacau

//print('Amostras de poligonos', features)
// print('Amostras de pontos', Amostrasdende)
// print('Amostra anterior', amostra_v01)


//Carregar ALOS
var alos = ee.ImageCollection("JAXA/ALOS/PALSAR/YEARLY/SAR");
//Carregar Sentinel-1 TOA reflectance data/
var s1 = ee.ImageCollection("COPERNICUS/S1_GRD");
//Carregar Sentinel-2 TOA reflectance data/
var s2 = ee.ImageCollection('COPERNICUS/S2');
//Carregar Landsat 8 TOA reflectance data/
var lsat8 = ee.ImageCollection('LANDSAT/LC08/C02/T1_TOA');



///////////////////////////////////////////////// Funções inserção de variaveis ////////////////////////////////////
//Função para rodar o filtro lee (ALOS 2 -PALSAR-2)
function filterleealos(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Homogenius Class - Enhanced Lee Filter)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLeealos =['HHLee','HVLee']
  var leefiltersalos = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefiltersalos.rename(BandsLeealos));
}

//Função para rodar o filtro lee (Sentinel 1)
function filterlee(image)
{
  // Filtro Lee para suavizar e reduzir os ruídos das imagens de Radar (Classe Homogênea - Filtro Lee Melhorado)
  var kernellee = ee.Kernel.square(3,'pixels');
  var BandsLee =['VVLee','VHLee']
  var leefilters = image.reduceNeighborhood({
    reducer:ee.Reducer.mean(),
    kernel:kernellee,
  });
  return image.addBands(leefilters.rename(BandsLee));
}


//Função para Adicionar o RVI (ALOS 2-PALSAR-2)
function addalosRVI(image)
{
  var rvialos = image.expression(
  '4 * hv / (hv + hh)',
  {
     hh: image.select('HHLee'),    
     hv: image.select('HVLee')
  });
  return image.addBands(rvialos.rename('RVIAlos'));
}
 
//Função para calcular e  Adicionar o RVI (Sentinel 1)
function addS1RVI(image)
{
  var rvi = image.expression(
  '4 * vh / (vh + vv)',
  {
     vv: image.select('VVLee'),    
     vh: image.select('VHLee')
  });
  return image.addBands(rvi.rename('RVI'));
}


//Função para calcular e  Adicionar o NDVI do Landsat 8
function addLandsatNDVI(image)
{
  var Landsat_NDVI = image.expression(
    '((ir-red)/(ir+red))',
    {
      ir: image.select('B5'),
      red: image.select('B4')
    });
    return image.addBands(Landsat_NDVI.rename('LS8_NDVI'));
}


//Função para calcular e inseri LAI (Leaf Area Index)
function addLAI(image)
{
    var ndvi = image.expression(
    '((red - ir) / (red + ir))',
    {
        red: image.select('B8'),    
        ir: image.select('B4')});
        
    var LAI = ndvi.expression('0.57 * exp(2.33 * b(0))').rename('LAI');

    image = image.addBands(LAI.rename('LAI'));
return image;
}



//Function to Run and add a GLCM algorithm over image
function addGLCMs2 (image){
  var glcm = image.select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'LAI']).toUint16().glcmTexture({size: 3})
  image = image.addBands(glcm)                                                
  return image;
}



//Function to Run and add a GLCM algorithm over sentinel 1 image
function addGLCMs1 (image){
  var glcm = image.select(['VV',
                            'VH',
                              'RVI']).toUint16().glcmTexture({size: 3})
  image = image.addBands(glcm)                                                
  return image;
}




// Function to run a Tasseled Cap Transformation 
// function addTasseledCap(image)
// {
//   var coefficients = ee.Array([
//     [0.3037, 0.2793, 0.4743, 0.5585, 0.5082, 0.1863],
//     [-0.2848, -0.2435, -0.5436, 0.7243, 0.0840, -0.1800],
//     [0.1509, 0.1973, 0.3279, 0.3406, -0.7112, -0.4572],
//     [-0.8242, 0.0849, 0.4398, -0.0580, 0.2013, -0.2768],
//     [-0.3289, -0.0545, 0.1070, -0.2933, -0.0241, 0.7730],
//     [-0.0260, -0.0167, 0.1286, -0.5774, 0.7269, 0.2062]
//   ]);
//   var mean = image.select('B2', 'B3', 'B4', 'B8', 'B11', 'B12').reduceRegion({
//     reducer: ee.Reducer.mean(),
//     geometry: image.geometry(),
//     scale: 10,
//     maxPixels: 1e9
//   });
//   var means = ee.Image.constant([mean.get('B2'), mean.get('B3'), mean.get('B4'), mean.get('B8'), mean.get('B11'), mean.get('B12')]);
//   var centered = image.select('B2', 'B3', 'B4', 'B8', 'B11', 'B12').subtract(means);
//   var arrayImage = centered.toArray();
//   var componentsImage = ee.Image(coefficients.matrixMultiply(arrayImage)).arrayProject([0]).arrayFlatten([['brightness', 'greenness', 'wetness', 'fourth', 'fifth', 'sixth']]);
//   return image.addBands(componentsImage);


// }





// Criação das máscara de nuvem usando Sentinel-2 QA60 band.
function maskS2clouds(image) {
  var qa = image.select('QA60');

// Bits 10 e 11 são nuvens e cirrus, respectivamente.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;
  
// Ambos os sinalizadores devem ser definidos como zero, indicando condições claras.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
             qa.bitwiseAnd(cirrusBitMask).eq(0));
  return image.updateMask(mask).divide(10000);
}





//Adicionando indices de vegetação
function addIndices(image){
  return image.addBands([
    image.select('B8','B4').normalizedDifference().multiply(10000).add(10000).rename('NDVI')]);
}

                     
// Fazer o empilhamento de imagens (raster stacking)
var alospalsar =  alos.filterDate('2017-06-01', '2018-10-31')
                  .select(['HH', 'HV'])
                  .map(filterleealos)
                  .map(addalosRVI);

var landsat8 =  lsat8.filterDate('2022-06-01', '2022-10-31')
                  .filterMetadata('CLOUD_COVER','less_than', 6)//25 -> 6% de nuvens nas imagens
                  .select(['B4', 'B5'])
                  .map(addLandsatNDVI);

var sentinel1 =  s1.filterDate('2022-06-01', '2022-10-31')
                  .select(['VV', 'VH'])
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VV'))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
                  .filter(ee.Filter.eq('instrumentMode', 'IW'))
                  .filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
                  .map(filterlee)
                  .map(addS1RVI)
                  .map(addGLCMs1);
                  
var sentinel2 = s2.filterDate('2022-06-01', '2022-10-31')
                  .filterMetadata('CLOUDY_PIXEL_PERCENTAGE','less_than', 6)//25 -> 6% de nuvens nas imagens 
                  .select(['B1',
                            'B2',
                              'B3',
                                'B4',
                                  'B5',
                                    'B6',
                                      'B7',
                                        'B8',
                                          'B8A',
                                            'B11',
                                              'B12',
                                                'QA60'])
                  .map(maskS2clouds)
                  .map(addIndices)
                  .map(addLAI)
                  .map(addGLCMs2)
                  //.map(addTasseledCap);
                  
var nome_bandas = image.bandNames();
// Composição de um conjunto de dados na área de estudo
var landsat8 = landsat8.median().clip(area_estudo);
var s1lee = sentinel1.median().clip(area_estudo);
var s2filtered = sentinel2.median().clip(area_estudo);
var aloslee = alospalsar.median().clip(area_estudo);
var stack = s2filtered.addBands(landsat8).addBands(s1lee).addBands(aloslee);

//Printing the bands of the stack
print('Bands of the stack: ', stack.bandNames());
//Printing bands added by glcm function
print('Bands added by glcm function: ', stack.bandNames().slice(12, 20));




// Mostrar o stack de imagens
Map.addLayer(stack, {bands: ['B11', 'B8', 'B4'], min: 0, max: 0.3}, 'stack', false);
//Map.addLayer(al2_composite, {bands: ['HH', 'HV'], min: 0, max: 8500}, 'ALOS2 composite', false);

//Getting all the bands of the stack and adding them to a list
var bands = stack.bandNames();






var bands = ['B1',
              'B2',
                'B3',
                  'B4',
                    'B5',
                      'B6',
                        'B7',
                          //'B8',
                            //'B8A',
                              'B11',
                                'B12',
                                  'LAI',
                                    //'LS8_NDVI',
                                      'VV',
                                        'VH',
                                          'RVI',
                                            'HH',
                                              'HV',
                                                'RVIAlos'];
var classe = 'Classe';
var presence = 'Map';



// On the Sentinel-2 image, run a Tasseled cap analysis
// var tasseledCap = stack.select(bands).tasseledCap({ coefficients: tcParams });
// var tasseledCap = tasseledCap.addBands(stack.select(bands));
// var tasseledCap = tasseledCap.addBands(stack.select(classe));
// var tasseledCap = tasseledCap.addBands(stack.select(presence));



// // //Add the texture and tasseled cap bands to the image
// var stack = stack.addBands(texture);
//var stack = stack.addBands(tasseledCap);




////////////////////////////////////////// Amostras //////////////////////////////////////////////////////////
var training = stack.select(bands).sampleRegions({
  collection: amostras,
  properties: [classe],
  scale: 30
});


var withRandom = training.randomColumn('random');

// 90% do banco de dados será utilizado para treino 
var split =  0.8
var trainingPartition = withRandom.filter(ee.Filter.lt('random', split));

var testingPartition = withRandom.filter(ee.Filter.gte('random', split));


// //////////////////////////////////////// Treinamentos dos Classificadores //////////////////////////////////////////


//RandomForest(RF)
// n_trees, variablespersplit, minleafpopulation
var trainedClassifier = ee.Classifier.smileRandomForest({numberOfTrees: 45,
          minLeafPopulation: 6,
          maxNodes: 5}).train({
          features:trainingPartition,
          classProperty: classe,
          inputProperties: bands
});





//Treinando o Classificador (Classification and Regression Trees - CART)
var trained = ee.Classifier.smileCart().train(trainingPartition, classe, bands);


// // // Retirando MaxEnt
// // // Definir e treinar o classificador Maxent de uma imagem por amostragens de pontos.
// // //var classifier = ee.Classifier.amnhMaxent().train({
// //   //features: trainingmaxent,
// //   //classProperty: 'presence',
// //   //inputProperties: stack.bandNames()
// // //});



// // ////////////////////////////////////////////// Imprimir os resultados /////////////////////////////////////////////

// // //Imprima algumas informações sobre o RF
print('Random Forest, explicado', trainedClassifier.explain());










// Calculando o quanto cada variável amostrada contribui através do Gini Importance
// Posteriormente, as variáveis serão plotadas em um gráfico de barras para visualização, de acordo com a importância relativa
var importance = ee.Dictionary(trainedClassifier.explain().get('importance'))
var sum = importance.values().reduce(ee.Reducer.sum())
var relativeImportance = importance.map(function(key, value) {
  return ee.Number(value).divide(sum).sort(relativeImportance.values())
});

print(relativeImportance, 'Importancia relativa');



var importance = ee.Dictionary(trainedClassifier.explain().get('importance'))
var sum = importance.values().reduce(ee.Reducer.sum())
var relativeImportance = importance.map(function(key, value) {
  return ee.Number(value).divide(sum).sort(relativeImportance.values())
})

print(relativeImportance, 'Importancia relativa');


var importanceFc = ee.FeatureCollection([
  ee.Feature(null, relativeImportance)
]);

// Plottando resultados em gráfico
var chart2 = ui.Chart.feature.byProperty({
  features: importanceFcs
}).setOptions({
      title: 'RF Importancia das variaveis',
      vAxis: {title: 'Importancia'},
      hAxis: {title: 'Bandas utilizadas'}
  });
print(chart2, 'Importancia relativa');



//

bands = ['B1',
'B11',
'B12',
'B2',
'B3',
'B4',
'B4_1',
'B5',
'B5_1',
'B6',
'B7',
'B8',
'HH',
'HHLee',
'HV',
'HVLee',
'LAI',
'LAI_contrast',
'LAI_corr',
'LAI_dent',
'LAI_diss',
'LAI_dvar',
'LAI_ent',
'LAI_idm',
'LAI_inertia',
'LAI_prom',
'LAI_savg',
'LAI_sent',
'LAI_svar',
'LAI_var',
'LS8_NDVI',
'NDVI',
'RVI',
'RVIAlos',
'VH',
'VHLee',
'VV',
'VVLee']



// // // //Imprima algumas informações sobre o CART
// // // print('CART, explicado', trained.explain());

// // //////////////////////////////////////////// Classificadores //////////////////////////////////////////////////////

// // //Classifique o composto (Random Forest)
var classificado = stack.classify(trainedClassifier);

// //Classifique o composto (CART)
// var classificado_cart = stack.select(bands).classify(trained);


// Classifica o teste na Coleção de Feições (Random Forest)
var test = testingPartition.classify(trainedClassifier);

// // Classifica o teste na Coleção de Feições (CART)
//var testCart = testingPartition.classify(trained);

// ////////////////////////////////////////// Visualizar Camadas ///////////////////////////////////////////////////

var vis = {
  bands: ['Map'],
};

var opacity = 0.5
var vis_color = {color: ''}
var shown = true

// // Adicionando Camadas 
Map.centerObject(area_estudo,8);
// Map.addLayer(classificado_cart, {min: 1, max: 2, palette: ['green','blue']},'CART');
Map.addLayer(classificado, {min: 1, max: 2, palette: ['grey','blue']},'Random Forest', shown, opacity);
//Map.addLayer(todas_classes, vis, 'todas as classes de uso Copernicus (10m)')
// Map.addLayer(limite_propriedades, {color: 'red'}, 'Limite das propriedades');
Map.addLayer(sample_cacau, {color: 'brown'}, 'Pontos amostras cacau');
// Map.addLayer(sample_pastagem, {color: 'yellow'}, 'Pontos amostras de pastagem');
// Map.addLayer(sample_floresta, {color: 'purple'}, 'Pontos amostras de floresta');
// Map.addLayer(limite_propriedades, {color: 'blue'}, 'Limites das propriedades rurais')
Map.addLayer(cacau, {color: 'brown'}, 'Polígonos de cacau sombreado');
// Adding a layer to show the ndvi image from the landsat 8 satellite
Map.addLayer(ndvi, {min: 0, max: 1, palette: ['blue', 'white', 'green']}, 'NDVI');


//Plotting results from the Tasseled Cap Transformation, NDVI and VH/VV bands





// //////////////////////////////////////////Filtro Majoritário na Classificação////////////////////////////

// // CART
// var kernel = ee.Kernel.square(2);
// var fmajoritclasscart = classificado_cart.reduceNeighborhood({
//   reducer:ee.Reducer.mode(),
//   kernel:kernel,
// });



// // Random Forest
// var fmajoritclassrf = classificado.reduceNeighborhood({
//   reducer:ee.Reducer.mode(),
//   kernel:kernel,
// });

// //Adicionando a camada
// //Map.addLayer(fmajoritclasscart,{min: 1, max: 2, palette: ['black', 'green']},'Majoritario_CART');
// //Map.addLayer(fmajoritclassrf,{min: 1, max: 2, palette: ['black', 'green']},'Majoritario_RF');




//////////////////////////////////////////// Validação /////////////////////////////////////////////////////
// //Random Forest
var MatrizErro = test.errorMatrix(classe, 'classification');
var AG = MatrizErro.accuracy();
var Kappa = MatrizErro.kappa();
print('Acurácia geral da validação - RF', AG);
print('Kappa - RF',Kappa);
//Calculate Omission Error and Commission Error
var OmissionError = MatrizErro.omissionError();
var CommissionError = MatrizErro.commissionError();

// //Classification and Regression Trees (CART)
// var MatrizErroCart = testCart.errorMatrix(classe, 'classification');
// var AG = MatrizErroCart.accuracy()
// var Kappa = MatrizErroCart.kappa()

// print('Acurácia geral da validação - CART', AG)
// print('Kappa - CART',Kappa)

// ///////////////////////////////////////Visualização dos Modelos////////////////////////////////////////////

// //Selecionar as imagens 
// var images=[
//   ee.ImageCollection(classificado),
//   ee.ImageCollection(classificado_cart),
//   //ee.ImageCollection(zones),
//   ee.ImageCollection(stack)
//   ];

// var vis = [
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 1, max: 2, palette: ['#000000','#DC143C']},
//   {min: 0, max: 0.3, bands: ['B4', 'B3', 'B2']}
// ];

// var NAMES = [
//   'Random Forest',
//   'CART',
//   //'MaxEnt',
//   'Imagens'
// ];

// // Cria um mapa para cada opção de visualização.
// var maps = [];
// NAMES.forEach(function(name,index) {
//   var map = ui.Map();
//   map.add(ui.Label(name));
//   map.addLayer(images[index], vis[index], name);
//   map.setControlVisibility(true);
//   maps.push(map);
// });

// var linker = ui.Map.Linker(maps);
// // Habilita o Zoom no mapa da esquerda "Antes".
// maps[0].setControlVisibility({zoomControl: true});
// // Mostra a escala (por exemplo, 500m) no mapa da direita.
// maps[0].setControlVisibility({scaleControl: true});
// maps[1].setControlVisibility({scaleControl: true});
// maps[2].setControlVisibility({scaleControl: true});
// //maps[3].setControlVisibility({scaleControl: true});

// // Cria o título.
// var title = ui.Label('Modelos - Cacau', {
//   stretch: 'horizontal',
//   textAlign: 'center',
//   fontWeight: 'bold',
//   fontSize: '24px',
//   color: '#0000FF'
// });

// // Cria um painel para armazenar a interface gráfica.
// // var panel = ui.Panel();
// // panel.style().set('width', '300px');

// // Cria uma grade de mapas.
// // var mapGrid = ui.Panel([
// //     ui.Panel([maps[0], maps[1]], null, {stretch: 'both'}),
// //     ui.Panel(maps[2], null, {strech: 'vertical'})
// //   ],
// //   ui.Panel.Layout.Flow('horizontal'), {stretch: 'both'}
// // );

// //Imagens: Layer error: Image.visualize: Cannot provide a palette when visualizing more than one band.
// // Adiciona os mapas e títulos na "ui.root".
// // ui.root.widgets().reset([title, mapGrid]);
// // ui.root.setLayout(ui.Panel.Layout.Flow('vertical'));

// // // Centraliza os mapas.
// // maps[0].setCenter(-53.7296, -4.553,10);

// // ui.Map.Linker([maps[0], maps[1], maps[2]]);

// //////////////////////////////////////////// Exportando resultados das Classificações //////////////////////////////////
// //CART
// Export.image.toDrive({
//   image:fmajoritclasscart,
//   description:'Cacau_CART_sol',
//   scale:10,
//   folder:'GEE_export',
//   region:area_estudo,
//   maxPixels:1e13
// });

// // MaxEnt
// // Export.image.toDrive({
// //   image:fmajoritclassmax,
// //   description:'Cacau_MaxEnt_sol',
// //   scale:10,
// //   folder:'GEE_export',
// //   region:area_estudo,
// //   maxPixels:1e13
// // });

//Random forest 
// Export.image.toDrive({
//   image:fmajoritclassrf,
//   description:'Cacau_RF_sol',
//   scale:10,
//   folder:'GEE_export',
//   region:area_estudo,
//   maxPixels:1e13
// });

// //Amostras
// Export.table.toDrive({
//   collection: dende,
//   description:'Cacau_Amostras_pol_sol',
//   folder: 'GEE_export',
//   fileFormat: 'KML'
// });


//Creating a layer to calculate the ndvi
// This area of interest is the south american continent
var south_america = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017').filter(ee.Filter.eq('wld_rgn', 'South America'));
//Calculating the NDVI from Sentinel 2
var s2 = ee.ImageCollection('COPERNICUS/S2')

var image = ee.Image(
s2.filterBounds(south_america)
.filterDate('2021-01-01', '2021-12-31')
.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
.sort('CLOUDY_PIXEL_PERCENTAGE')
.first()
)

//reducing the image by calculating the mean
var image = image.reduce(ee.Reducer.mean())


Map.addLayer(ndvi, {min: 0, max: 1}, 'NDVI');

//Exporting the NDVI image
Export.image.toDrive({
  image: ndvi,
  description: 'NDVI',
  scale: 30,
  region: south_america,
  maxPixels: 1e13
});